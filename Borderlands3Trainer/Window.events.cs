﻿using Borderlands3Trainer.cheats;
using Borderlands3Trainer.controls;
using Borderlands3Trainer.Exception;
using Borderlands3Trainer.util;
using gitlabupdater;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands3Trainer
{
    partial class Window
    {
        private AutoUpdateWindowConfirm upwindow = new AutoUpdateWindowConfirm();
        private Updater updater = new Updater("xize/borderlands3trainer", Application.ProductVersion);
        private BackgroundWorker w;
        private bool updatefound = false;

        private void formcompleted(object sender, EventArgs e)
        {
            this.SetupUpdate();
        }

        //setup this method once per day, so we are not giving huge hits to gitlab :)
        private void SetupUpdate()
        {
            String datafolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "/0c3";
            if (!Directory.Exists(datafolder))
            {
                Directory.CreateDirectory(datafolder);
            }

            if (!Directory.Exists(datafolder + "/borderlands3trainer"))
            {
                Directory.CreateDirectory(datafolder + "/borderlands3trainer");
            }

            if (File.Exists(datafolder + "/borderlands3trainer/auto-update.dat"))
            {
                long dat = long.Parse(File.ReadAllText(datafolder + "/borderlands3trainer/auto-update.dat"));
                DateTime time = new DateTime(dat);
                DateTime now = DateTime.Now;
                if (now.Subtract(time).TotalDays <= 7)
                {
                  return;
                }
            }

            File.WriteAllText(datafolder + "/borderlands3trainer/auto-update.dat", DateTime.Now.Ticks + "");


            this.w = new BackgroundWorker();
            w.WorkerReportsProgress = true;
            w.DoWork += new DoWorkEventHandler(AutoUpdateDoWork);
            w.ProgressChanged += new ProgressChangedEventHandler(AutoUpdateProgress);
            w.RunWorkerCompleted += new RunWorkerCompletedEventHandler(AutoUpdateCompleted);
            w.RunWorkerAsync();
        }

        private void AutoUpdateDoWork(object sender, DoWorkEventArgs e)
        {
            w.ReportProgress(30);
            if(updater.Verify())
            {
                w.ReportProgress(50);
            } else
            {
                for (int i = 0; i < 180; i++)
                {
                    //check for different update :)
                    this.updater = new Updater("xize/borderlands3trainer", Application.ProductVersion.Replace("349", 349+i+""));
                    if (updater.Verify())
                    {
                        w.ReportProgress(50);
                    }
                }
            }
            w.ReportProgress(100);
            Thread.Sleep(1200);
        }

        private void AutoUpdateProgress(object sender, ProgressChangedEventArgs e)
        {
            if(e.ProgressPercentage == 50)
            {
                this.updatefound = !this.updatefound;
            }
        }

        private void AutoUpdateCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if(this.updatefound)
            {
                upwindow.NewVersion = this.updater.NewVersion;
                upwindow.OldVersion = Application.ProductVersion;
                upwindow.Updater = this.updater;
                Point loc = this.Location;
                loc.Offset(90, 150);
                upwindow.Show();
                upwindow.Location = loc;
                upwindow.Focus();
            }
        }


        private void godmodecheckbox_CustomCheckBoxChange(object sender, CustomCheckBoxEventArgs e)
        {
            if(e.CurrentState().Equals(CustomCheckBox.CheckState.CHECKED))
            {       
                IPlayer p = Trainer.GetPlayer();
                Task<IEnumerable<long>> t = this.m.AoBScan(CheatOffsetType.HEALTH.Offset, true, true);
                t.ConfigureAwait(true);
                t.Wait();
                long addr = t.Result.FirstOrDefault();
                String address = Convert.ToString(addr, 16).ToUpper();
                if(address != "0x0")
                {
                    p.GodmodeAddress = address;
                } else
                {
                    Speech.Speak(this.mute, "failed to enable godmode");
                    e.Cancel = true;
                }  
            }
        }


        private void shieldcheckbox_CustomCheckBoxChange(object sender, CustomCheckBoxEventArgs e)
        {
            if(e.CurrentState().Equals(CustomCheckBox.CheckState.CHECKED))
            {
                IPlayer p = Trainer.GetPlayer();
                Task<IEnumerable<long>> t = this.m.AoBScan(CheatOffsetType.SHIELD.Offset, true, true);
                t.ConfigureAwait(true);
                t.Wait();

                long addr = t.Result.FirstOrDefault();
                String address = Convert.ToString(addr, 16).ToUpper();
                if (address != "0x0")
                {
                    p.InfinitiveShieldAddress = address;
                } else
                {
                    Speech.Speak(this.mute, "failed to enable infinitive shield");
                    e.Cancel = true;
                }
            }
        }

        private void grenadecheckbox_CustomCheckBoxChange(object sender, CustomCheckBoxEventArgs e)
        {
            if (e.CurrentState().Equals(CustomCheckBox.CheckState.CHECKED))
            {
                IPlayer p = Trainer.GetPlayer();
                Task<IEnumerable<long>> t = this.m.AoBScan(CheatOffsetType.GRENADES.Offset, true, false);
                t.ConfigureAwait(true);
                t.Wait();

                long addr = t.Result.FirstOrDefault();
                String address = Convert.ToString(addr, 16).ToUpper();
                if(address != "0x0")
                {
                    p.InfinitiveGrenadeAddress = address;
                } else
                {
                    Speech.Speak(this.mute, "failed to enable infinitive grenades");
                    e.Cancel = true;
                }
            }
        }

        private void setlocationtpmenu(object sender, EventArgs e)
        {
            String name = ((ToolStripMenuItem)sender).Text;

            this.teleportbox.Text = name;

        }
        private void tpbutton_Click(object sender, EventArgs v)
        {
            try
            {
                this.DoTeleport();
            } catch(System.Exception e)
            {
                String notepadargs =
                        Application.ProductName + " " + Application.ProductVersion + " generated a exception!" +
                        "\n\nName of Cheat: " + CheatOffsetType.Y_COORD.Name +
                        "\nEnvolved offset: " + CheatOffsetType.Y_COORD.Offset +
                        "\n\nException name: ```" + e.Message + "```\n\n\nStacktrace:\n```" + e.StackTrace + "```\n\n" +
                        "post this information to https://gitlab.com/xize/borderlands3trainer/-/issues \nand what cheat would have caused this!";

                TrainerException ex = new TrainerException();
                ex.StartPosition = FormStartPosition.CenterParent;
                ex.StackTrace = notepadargs;
                ex.SmallExceptionMessage = "Name of Cheat: "+ CheatOffsetType.Y_COORD.Name +
                "\nEnvolved offset: " + CheatOffsetType.Y_COORD.Offset;
                ex.ExceptionMessage = e.Message;
                ex.Exception = e;

                ex.ShowDialog();
            }
        }

        private void Tpundo_Click(object sender, EventArgs v)
        {
            try
            {
                this.Trainer.GetPlayer().UndoTeleport();
            } catch(System.Exception e)
            {
                String notepadargs =
                    Application.ProductName + " " + Application.ProductVersion + " generated a exception!" +
                    "\n\nName of Cheat: " + CheatOffsetType.Y_COORD.Name +
                    "\nEnvolved offset: " + CheatOffsetType.Y_COORD.Offset +
                    "\n\nException name: ```" + e.Message + "```\n\n\nStacktrace:\n```" + e.StackTrace + "```\n\n" +
                    "post this information to https://gitlab.com/xize/borderlands3trainer/-/issues \nand what cheat would have caused this!";

                TrainerException ex = new TrainerException();
                ex.StartPosition = FormStartPosition.CenterParent;
                ex.StackTrace = notepadargs;
                ex.SmallExceptionMessage = "Name of Cheat: " + CheatOffsetType.Y_COORD.Name +
                "\nEnvolved offset: " + CheatOffsetType.Y_COORD.Offset;
                ex.ExceptionMessage = e.Message;
                ex.Exception = e;

                ex.ShowDialog();
            }
        }

        private void Teleportsetbtn_Click(object sender, EventArgs e)
        {
            this.tpmanager.StartPosition = FormStartPosition.CenterScreen;
            this.tpmanager.Show();
        }

        private void Moneybtn_Click(object sender, EventArgs v)
        {
            try
            {
              //  this.Trainer.GetPlayer().Money = this.Trainer.GetPlayer().Money + Decimal.ToInt32(this.moneynumeric.Value);
            } catch(System.Exception e)
            {
                String notepadargs =
                    Application.ProductName + " " + Application.ProductVersion + " generated a exception!" +
                    "\n\nName of Cheat: " + CheatOffsetType.MONEY.Name +
                    "\nEnvolved offset: " + CheatOffsetType.MONEY.Offset +
                    "\n\nException name: ```" + e.Message + "```\n\n\nStacktrace:\n```" + e.StackTrace + "```\n\n" +
                    "post this information to https://gitlab.com/xize/borderlands3trainer/-/issues \nand what cheat would have caused this!";

                TrainerException ex = new TrainerException();
                ex.StartPosition = FormStartPosition.CenterParent;
                ex.StackTrace = notepadargs;
                ex.SmallExceptionMessage = "Name of Cheat: " + CheatOffsetType.MONEY.Name +
                "\nEnvolved offset: " + CheatOffsetType.MONEY.Offset;
                ex.ExceptionMessage = e.Message;
                ex.Exception = e;

                ex.ShowDialog();
            }
        }

        private void Eridiumbtn_Click(object sender, EventArgs v)
        {
            try
            {
                //this.Trainer.GetPlayer().Eridium = this.Trainer.GetPlayer().Eridium + Decimal.ToInt32(this.eridiumnumeric.Value);
            } catch(System.Exception e)
            {
                String notepadargs =
                    Application.ProductName + " " + Application.ProductVersion + " generated a exception!" +
                    "\n\nName of Cheat: " + CheatOffsetType.ERIDIUM.Name +
                    "\nEnvolved offset: " + CheatOffsetType.ERIDIUM.Offset +
                    "\n\nException name: ```" + e.Message + "```\n\n\nStacktrace:\n```" + e.StackTrace + "```\n\n" +
                    "post this information to https://gitlab.com/xize/borderlands3trainer/-/issues \nand what cheat would have caused this!";

                TrainerException ex = new TrainerException();
                ex.StartPosition = FormStartPosition.CenterParent;
                ex.StackTrace = notepadargs;
                ex.SmallExceptionMessage = "Name of Cheat: " + CheatOffsetType.ERIDIUM.Name +
                "\nEnvolved offset: " + CheatOffsetType.ERIDIUM.Offset;
                ex.ExceptionMessage = e.Message;
                ex.Exception = e;

                ex.ShowDialog();
            }
        }

        private void Goldkeybtn_Click(object sender, EventArgs e)
        {
            MessageBox.Show("golden keys are broken");
            //this.Trainer.GetPlayer().GoldenKey = this.Trainer.GetPlayer().GoldenKey + Decimal.ToInt32(this.goldkeynumeric.Value);
        }

    }
}
