﻿using Borderlands3Trainer.cheats;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands3Trainer
{
    public partial class TrainerExceptionBaseForm : Form
    {
        private String datafolder = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "/0c3/borderlands3trainer";
        private System.Exception e;
        private String fullex;

        public TrainerExceptionBaseForm()
        {
            InitializeComponent();
        }

        public System.Exception Exception
        {
            get
            {
                return this.e;
            }
            set
            {
                this.e = value;
            }
        }

        public String ExceptionMessage
        {
            get { return this.exception.Text; }
            set { this.exception.Text = value; }
        }

        public String SmallExceptionMessage
        {
            get { return this.stacktrace.Text; }
            set { this.stacktrace.Text = value; }
        }

        public String StackTrace
        {
            get { return this.fullex; }
            set { this.fullex = value;}
        }

        private void TrainerExceptionBaseForm_Load(object sender, EventArgs e)
        {
            this.moveAbleWindow1.ControlTarget = new Control[] { this.panel1, this };
        }

        private void cancelbtn_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();    
        }

        private void reportbtn_Click(object sender, EventArgs e)
        {
            
            if(!Directory.Exists(this.datafolder + @"\logs"))
            {
                Directory.CreateDirectory(this.datafolder+@"\logs");
            }

            String filename = this.datafolder + @"\logs\bl3trainer-" + DateTime.Now.ToString("hmsdmy") + ".log";
            File.WriteAllText(filename, this.StackTrace);

            Clipboard.SetText(this.StackTrace);
            Process p2 = Process.Start("explorer", "https://gitlab.com/xize/borderlands3trainer/-/issues");

            p2.WaitForExit();

            this.DialogResult = DialogResult.OK;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Abort;
            Application.Exit();
        }
    }
}
