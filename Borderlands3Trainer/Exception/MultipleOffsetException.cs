﻿using Borderlands3Trainer.cheats;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Borderlands3Trainer.Exception
{
    [Serializable]
    class MultipleOffsetException : System.Exception
    {

        private String name;
        private String offset;

        public MultipleOffsetException(String inneroffsetname, String offset) : base(String.Format("cheat {0} with multiple offsets failed to work!", inneroffsetname))
        {
            this.name = inneroffsetname;
            this.offset = offset;
        }

        /**
         * <summary>the name of the entity where this offset is ment for</summary>
         * */
        public String Name
        {
            get { return this.name; }
        }

        /**
         * <summary>the offset of the cheat being used</summary>
         * */
        public String Offset
        {
            get { return this.offset; }
        }

    }
}
