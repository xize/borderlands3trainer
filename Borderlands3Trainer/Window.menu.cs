﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands3Trainer
{
    partial class Window
    {

        private void GodmodeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.godmodecheckbox.Checked = !this.godmodecheckbox.Checked;
        }

        private void ShieldToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.shieldcheckbox.Checked = !this.shieldcheckbox.Checked;
        }


        private void InfinitiveGrenadesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.grenadecheckbox.Checked = !this.grenadecheckbox.Checked;
        }

        private void GiveMoneyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.moneybtn.PerformClick();
        }

        private void GiveEridiumToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.eridiumbtn.PerformClick();
        }

        private void GiveGoldenKeysToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.goldkeybtn.PerformClick();
        }

    }
}
