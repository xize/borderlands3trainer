﻿using Borderlands3Trainer.cheats;
using Borderlands3Trainer.util;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands3Trainer
{
    partial class Window
    {

        private TeleportManager tpmanager;

        public void DoTeleport()
        {
            TeleportType type = TeleportType.ValueOf((String)this.teleportbox.SelectedItem);

            Speech.Speak(this.mute, "teleporting to " + type.getVoiceName());

            if (type == TeleportType.EDEN_SIX_MERIDIAN_METROCOMPLEX_GIGAMIND)
            {
                this.Trainer.GetPlayer().TeleportTo(TeleportType.EDEN_SIX_MERIDIAN_METROCOMPLEX_GIGAMIND);
            }
            else if (type == TeleportType.EDEN_SIX_THE_ANVIL_WARDEN)
            {
                this.Trainer.GetPlayer().TeleportTo(TeleportType.EDEN_SIX_THE_ANVIL_WARDEN);
            }
            else if (type == TeleportType.EDEN_SIX_JACOB_REAL_ESTATE_DADDY)
            {
                this.Trainer.GetPlayer().TeleportTo(TeleportType.EDEN_SIX_JACOB_REAL_ESTATE_DADDY);
            }
            else if (type == TeleportType.EDEN_SIX_JACOB_REAL_ESTATE_ELDRAGON)
            {
                this.Trainer.GetPlayer().TeleportTo(TeleportType.EDEN_SIX_JACOB_REAL_ESTATE_DADDY);
            } else if(type == TeleportType.THE_HOUSE_THAT_JOEY_BUILD_BOSS)
            {
                this.Trainer.GetPlayer().TeleportTo(TeleportType.THE_HOUSE_THAT_JOEY_BUILD_BOSS);
            } else if(type == TeleportType.ATHENAS_CAPTAIN_TRAUNT)
            {
                this.Trainer.GetPlayer().TeleportTo(TeleportType.ATHENAS_CAPTAIN_TRAUNT);
            } else if (type == TeleportType.MIDNIGHT_CARIN_VALKYRIE_BOSS)
            {
                this.Trainer.GetPlayer().TeleportTo(TeleportType.MIDNIGHT_CARIN_VALKYRIE_BOSS);
            } else if (type == TeleportType.MIDNIGHT_CARIN_WOTAN_BOSS)
            {
                this.Trainer.GetPlayer().TeleportTo(TeleportType.MIDNIGHT_CARIN_WOTAN_BOSS);
            } else if (type == TeleportType.THE_HANDSOME_JACKPOT_COMPACTOR_SCRAPTRAP)
            {
                this.Trainer.GetPlayer().TeleportTo(TeleportType.THE_HANDSOME_JACKPOT_COMPACTOR_SCRAPTRAP);
            }
            else if (type == TeleportType.THE_HOUSE_THAT_JOEY_BUILD_SECRET_ROOM)
            {
                this.Trainer.GetPlayer().TeleportTo(TeleportType.THE_HOUSE_THAT_JOEY_BUILD_SECRET_ROOM);
            }
            else
            {
                try
                {
                    if (type is CustomTeleportType)
                    {
                        this.Trainer.GetPlayer().TeleportTo(type);
                    }
                }
                catch (System.Exception)
                {
                    //do nothing :)
                }
            }
        }

        public void RebuildTeleportList()
        {
            this.teleportbox.Items.Clear();
            foreach (TeleportType tel in TeleportType.Values())
            {
                this.teleportbox.Items.Add(tel.GetName());
            }
            this.teleportbox.SelectedIndex = 0;
        }

    }
}
