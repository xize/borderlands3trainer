﻿using Borderlands3Trainer.cheats;
using Borderlands3Trainer.util;
using Memory;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Text;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands3Trainer
{
    public partial class Window : Form
    {

        private NoteWindow notewin = new NoteWindow();
        private Borderlands3TrainerHelp helpwindw = new Borderlands3TrainerHelp();
        private Mem m = new Mem();

        public Window()
        {
            InitializeComponent();
        }

        private void Window2_Load(object sender, EventArgs e)
        {
            this.Hide();
            this.moveAbleWindow1.ControlTarget = new Control[]{ this.cursorholder };

            Trainer t = new Trainer(this, this.m);
            Trainer = t;
            if(!Debugger.IsAttached)
            {
                t.Start();
            }
            

            Console.Beep(500, 100);
            Console.Beep(200, 100);
            Console.Beep(500, 100);

            this.fadeControl1.Start();


            this.version.Text = String.Format(this.version.Text, Application.ProductVersion);
            this.FormClosing += new FormClosingEventHandler(onformclose);
             
            this.tpmanager = new TeleportManager(this, this.Trainer);


            //add teleport stuff
            foreach (TeleportType tel in TeleportType.Values())
            {
                this.teleportbox.Items.Add(tel.GetName());
            }

            this.teleportbox.Text = (String)teleportbox.Items[0];

            this.RegisterHotKeys();

        }

        private void onformclose(object sender, FormClosingEventArgs e)
        {
            this.Exit();
        }

        public void Exit()
        {
            Application.Exit();
        }

        private void Label1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Label2_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        public Trainer Trainer
        {
            get;set;
        }

        private void Label6_Click(object sender, EventArgs e)
        {
            Point currentpoint = this.Location;
            this.helpwindw.Opacity = 0.8;
            this.helpwindw.Location = new Point(currentpoint.X+20, currentpoint.Y+80);
            this.helpwindw.ShowDialog();
        }

        private void CreditsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Point currentpoint = this.Location;
            this.helpwindw.Opacity = 0.8;
            this.helpwindw.Location = new Point(currentpoint.X + 20, currentpoint.Y + 80);
            this.helpwindw.ShowDialog();
        }

        private void GithubToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Process.Start("explorer.exe", "https://gitlab.com/xize/borderlands3trainer/-/releases");
        }

        private void fadeControl1_FadeFinish(object sender, EventArgs e)
        {
            this.notewin.Show();
        }

    }
}
