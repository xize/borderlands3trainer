﻿
using Borderlands3Trainer;
using Borderlands3Trainer.util;
using Memory;
using System;
using System.Linq;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using Borderlands3Trainer.cheats;
using System.Text;
using Borderlands3Trainer.Exception;
using System.Runtime.CompilerServices;

namespace Borderlands3Trainer
{
    public class Trainer
    {

        private Mem m;
        private Window win;
        private BackgroundWorker worker = new BackgroundWorker();

        //cheats

        private IPlayer player;

        //end cheats

        public Trainer(Window win, Mem m)
        {
            this.win = win;
            this.m = m;

            //instance the cheats.
            this.InitializeCheats(m);
        }

        public void InitializeCheats(Mem m)
        {
            this.player = new PlayerModel(this.m);

        }

        public IPlayer GetPlayer()
        {
            return this.player;
        }

        public bool OpenGame()
        {

            int PID = this.m.GetProcIdFromName("Borderlands3");

            if (this.Hooked && PID > 0)
                return true;

            if (PID > 0)
            {
                if(!this.Hooked)
                {
                    bool bol = this.m.OpenProcess(PID);
                    if(bol)
                    {
                        this.Hooked = true;
                        return true;
                    }
                }
            } else {
                    this.Hooked = false;
            }
            return false;
        }


        public bool Hooked { get; private set; } = false;

        public Process Process { get { return this.m.theProc; } }

        public void Start()
        {
            if(!worker.IsBusy)
            {
                worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(OnBackgroundWorkerCompletedEvent);
                worker.DoWork += new DoWorkEventHandler(OnDoWorkEvent);
                worker.RunWorkerAsync();
            }
        }

        private void OnDoWorkEvent(Object sender, DoWorkEventArgs e)
        {
            //read only is accepted on a synchronized thread so we keep this, however if we want to write values to the form this will fail.
            while(true)
            {
                this.OpenGame();


                if (!this.Hooked)
                    return;

                IPlayer p = this.GetPlayer();

                if (this.win.godmodecheckbox.Checked)
                {
                    p.DoGodmode();
                }

                if(this.win.shieldcheckbox.Checked)
                {
                    p.DoInfinitiveShield();
                }

                if(this.win.grenadecheckbox.Checked)
                {
                    p.DoInfinitiveGrenades();
                }

                if (this.win.infammocheckbox.Checked)
                {
                    p.DoInfinitiveAmmo();
                }
            }
        }

        private void OnBackgroundWorkerCompletedEvent(object sender, RunWorkerCompletedEventArgs e)
        {
            
            if(e.Error != null)
            {
                CheatName n = null;
                CheatOffsetType offset = null;

                MethodBase methodb = e.Error.TargetSite;

                MultipleOffsetException ammoex = null;

                if (methodb.GetCustomAttributes<CheatName>(true).Any())
                {
                    n = methodb.GetCustomAttributes<CheatName>().FirstOrDefault();
                    switch (n.Name)
                    {
                        case "Health":
                            offset = CheatOffsetType.HEALTH;
                            this.win.godmodecheckbox.Checked = false;
                            break;
                        case "Shield":
                            offset = CheatOffsetType.SHIELD;
                            this.win.shieldcheckbox.Checked = false;
                            break;
                        case "Grenades":
                            offset = CheatOffsetType.GRENADES;
                            this.win.grenadecheckbox.Checked = false;
                            break;
                        case "Ammo":
                            offset = CheatOffsetType.AMMO;
                            ammoex = (MultipleOffsetException)e.Error;
                            this.win.infammocheckbox.Checked = false;
                            break;
                        case "Teleport to location":
                            offset = CheatOffsetType.Y_COORD;
                            break;
                        case "Teleport To":
                            offset = CheatOffsetType.Y_COORD;
                            break;
                        case "Teleport Undo":
                            offset = CheatOffsetType.Y_COORD;
                            break;
                        case "Eridium":
                            offset = CheatOffsetType.ERIDIUM;
                            break;
                        case "Golden Key":
                            offset = CheatOffsetType.GOLDEN_KEYS;
                            break;
                        case "Money":
                            offset = CheatOffsetType.MONEY;
                            break;
                    }
                }

                String notepadargs = "";

                TrainerException ex = new TrainerException();
                ex.StartPosition = FormStartPosition.CenterParent;
                

                if (ammoex != null)
                {
                    notepadargs =
                        Application.ProductName + " " + Application.ProductVersion + " generated a exception!" +
                        "\n\nName of multiple offset cheat: " + ammoex.Name +
                        "\nEnvolved target offset: " + ammoex.Offset +
                        "\n\nException name: ```" + e.Error.Message + "```\n\n\nStacktrace:\n```" + e.Error.StackTrace + "```\n\n" +
                        "post this information to https://gitlab.com/xize/borderlands3trainer/-/issues \nand what cheat would have caused this!";

                        ex.StackTrace = notepadargs;
                    ex.SmallExceptionMessage = "Name of Cheat: " +ammoex.Name +
                "\nEnvolved offset: " + ammoex.Offset;

                }
                else
                {
                    notepadargs =
                        Application.ProductName + " " + Application.ProductVersion + " generated a exception!" +
                        "\n\nName of Cheat: " + (n != null ? n.Name : "not known") +
                        "\nEnvolved offset: " + (offset != null ? (!offset.SingleOffset ? "\n" + this.FormatArrayString(offset.Offsets) : "\n" + offset.Offset) : "not known") +
                        "\n\nException name: ```" + e.Error.Message + "```\n\n\nStacktrace:\n```" + e.Error.StackTrace + "```\n\n" +
                        "post this information to https://gitlab.com/xize/borderlands3trainer/-/issues \nand what cheat would have caused this!";

                    ex.StackTrace = notepadargs;
                    ex.SmallExceptionMessage = "Name of Cheat: " + (n != null ? n.Name : "not known") +
                    "\nEnvolved offset: " + (offset != null ? (!offset.SingleOffset ? this.FormatArrayString(offset.Offsets) : offset.Offset) : "not known");
                }
                
                
                ex.ExceptionMessage = e.Error.Message;
                ex.Exception = e.Error;
                DialogResult r = ex.ShowDialog();
                if(r == DialogResult.OK)
                {
                    this.Start();
                }
            }
        }

        private String FormatArrayString(String[] data)
        {
            StringBuilder b = new StringBuilder();
            foreach (String d in data)
            {
                b.AppendLine(d);
                b.AppendLine();
            }
            return b.ToString();
        }

    }
}
