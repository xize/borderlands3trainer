﻿using Borderlands3Trainer.cheats;
using Borderlands3Trainer.util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands3Trainer
{
    partial class Window
    {


        [DllImport("user32.dll", SetLastError = true)]
        static extern uint GetWindowThreadProcessId(IntPtr hWnd, out int lpdwProcessId);

        [DllImport("user32.dll")]
        static extern IntPtr GetForegroundWindow();

        private HotKeyProvider hotkey;

        public void RegisterHotKeys()
        {
            this.hotkey = new HotKeyProvider(this);
            hotkey.RegisterKey(Keys.NumPad0);
            hotkey.RegisterKey(Keys.NumPad1);
            hotkey.RegisterKey(Keys.NumPad2);
            hotkey.RegisterKey(Keys.NumPad3);
            hotkey.RegisterKey(Keys.NumPad4);
            //hotkey.RegisterKey(Keys.NumPad5);
            //hotkey.RegisterKey(Keys.NumPad6);
            hotkey.RegisterKey(Keys.Left);
            hotkey.RegisterKey(Keys.Right);
            hotkey.RegisterKey(Keys.Down);
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);

            if (m.Msg == 0x0312)
            {
                /* Note that the three lines below are not needed if you only want to register one hotkey.
                 * The below lines are useful in case you want to register multiple keys, which you can use a switch with the id as argument, or if you want to know which key/modifier was pressed for some particular reason. */


                int id = m.WParam.ToInt32();

                Keys key = (Keys)(((int)m.LParam >> 16) & 0xFFFF);                  // The key of the hotkey that was pressed.

                if (this.Trainer.Process == null)
                {
                    Speech.Speak("Error!, the process was not found!");
                    return;
                }

                //use GetWindowThreadProcessId to check wether the focus is on the trainer or the game and not a completely different window.
                int processout;
                GetWindowThreadProcessId(GetForegroundWindow(), out processout);

                if (this.Trainer.Process.Id == processout || Process.GetCurrentProcess().Id == processout)
                {
                    if (key == Keys.NumPad0)
                    {
                        if (this.godmodecheckbox.Checked)
                        {
                            this.godmodecheckbox.PerformClick();
                            Speech.Speak(this.mute, "godmode disabled");
                        }
                        else
                        {
                            if(this.godmodecheckbox.Enabled)
                            {
                                this.godmodecheckbox.PerformClick();
                                Speech.Speak(this.mute, "Enabling godmode");
                            } else
                            {
                                Console.Beep(200, 100);
                                Console.Beep(300, 100);
                                Console.Beep(100, 100);
                                Speech.Speak("Error: cannot run godmode cheat because we did not complete the array of byte signature scan.");
                            }
                            
                        }
                    }
                    else if (key == Keys.NumPad1)
                    {
                        if (this.shieldcheckbox.Checked)
                        {
                            this.shieldcheckbox.PerformClick();
                            Speech.Speak(this.mute, "infinitive shield disabled");
                        }
                        else
                        {
                            if(this.shieldcheckbox.Enabled)
                            {
                                this.shieldcheckbox.PerformClick();
                                Speech.Speak(this.mute, "Enabling infinitive Shield");
                            } else
                            {
                                Console.Beep(200, 100);
                                Console.Beep(300, 100);
                                Console.Beep(100, 100);
                                Speech.Speak("Error: cannot run infinitive shield cheat because we did not complete the array of byte signature scan.");
                            }
                        }
                    }
                    else if (key == Keys.NumPad2)
                    {
                        if (this.grenadecheckbox.Checked)
                        {
                            this.grenadecheckbox.PerformClick();
                            Speech.Speak(this.mute, "infinitive grenades disabled");
                        }
                        else
                        {
                            if(this.grenadecheckbox.Enabled)
                            {
                                this.grenadecheckbox.PerformClick();
                                Speech.Speak(this.mute, "Enabling infinitive grenades");
                            } else
                            {
                                Console.Beep(200, 100);
                                Console.Beep(300, 100);
                                Console.Beep(100, 100);
                                Speech.Speak("Error: cannot run infinitive grenade cheat because we did not complete the array of byte signature scan.");
                            }
                        }
                    }
                    else if (key == Keys.NumPad3)
                    {
                        if (this.infammocheckbox.Checked)
                        {
                            this.infammocheckbox.PerformClick();
                            Speech.Speak(this.mute, "infinitive ammo disabled");
                        }
                        else
                        {
                            this.infammocheckbox.PerformClick();
                            Speech.Speak(this.mute, "infinitive ammo enabled");
                        }
                    }
                    else if (key == Keys.NumPad4)
                    {
                        this.DoTeleport();
                    }
                    else if (key == Keys.Right)
                    {
                        int index = this.teleportbox.SelectedIndex;

                        if ((index + 1) <= this.teleportbox.Items.Count - 1)
                        {
                            this.teleportbox.SelectedIndex = index + 1;
                            this.teleportbox.Text = (String)this.teleportbox.SelectedItem;
                            TeleportType type = TeleportType.ValueOf((String)this.teleportbox.SelectedItem);
                            Speech.Speak("switching next to option: " + type.getVoiceName());
                        }
                        else
                        {
                            Console.Beep();
                        }
                    }
                    else if (key == Keys.Left)
                    {
                        int index = this.teleportbox.SelectedIndex;

                        if ((index - 1) >= 0)
                        {
                            this.teleportbox.SelectedIndex = index - 1;
                            this.teleportbox.Text = (String)this.teleportbox.SelectedItem;
                            TeleportType type = TeleportType.ValueOf((String)this.teleportbox.SelectedItem);
                            Speech.Speak("switching back to option: " + type.getVoiceName());
                        }
                        else
                        {
                            Console.Beep();
                        }
                    }
                    else if (key == Keys.Down)
                    {
                        this.tpundo.PerformClick();
                    }
                    else if (key == Keys.NumPad5)
                    {
                        Console.Beep();
                        this.moneybtn.PerformClick();
                    }
                    else if (key == Keys.NumPad6)
                    {
                        Console.Beep();
                        this.eridiumbtn.PerformClick();
                    }
                    else if (key == Keys.NumPad7)
                    {
                        Console.Beep(80,900);
                        this.goldkeybtn.PerformClick();
                    }
                }
            }
        }

        public void UnRegisterHotKeys()
        {
            this.hotkey.UnRegisterAll();
        }

        private void Disablehotkeyscheckbox_CheckedChanged(object sender, EventArgs e)
        {
            if (this.disablehotkeyscheckbox.Checked)
            {
                this.UnRegisterHotKeys();
            }
            else
            {
                this.RegisterHotKeys();
            }
        }

        private void Mute_CheckedChanged(object sender, EventArgs e)
        {
            if (this.mute.Checked)
            {
                this.mute.ForeColor = Color.Green;
            }
            else
            {
                this.mute.ForeColor = Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            }
        }

    }
}
