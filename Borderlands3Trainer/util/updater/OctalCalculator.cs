﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gitlabupdater
{
    public class OctalCalculator
    {

        public String IncrementVersion(String version)
        {
            String[] data = version.Split('.');
            int octal1 = int.Parse(data[0]);
            int octal2 = int.Parse(data[1]);
            int octal3 = int.Parse(data[2]);
            int octal4 = int.Parse(data[3]);

            if(octal4 >= 9)
            {
                if (!(octal4 > 9))
                {
                    octal4 = 0;
                }
                if(octal3 == 9)
                {
                    if (!(octal3 > 9))
                    {
                        octal3 = 0;
                    }
                    if(octal2 == 9)
                    {
                        if (!(octal2 > 9))
                        {
                            octal2 = 0;
                        }
                        if (octal1 == 9)
                        {
                            octal1 = octal1 + 0;
                        } else
                        {
                            octal1++;
                        }
                    } else
                    {
                        octal2++;
                    }
                } else
                {
                    octal3++;
                }
            } else
            {
                octal4++;
            }

            return octal1 + "." + octal2 + "." + octal3 + "." + octal4;
        }

        public String IncrementFixedVersion(String version, int octaltarget, bool autoincrementafter = true)
        {
            String[] data = version.Split('.');
            int octal1 = int.Parse(data[0]);
            int octal2 = int.Parse(data[1]);
            int octal3 = int.Parse(data[2]);
            int octal4 = int.Parse(data[3]);

            if(octaltarget == 4)
            {
                octal4++;
            } else if(octaltarget == 3)
            {
                octal3++;
            } else if(octaltarget == 2)
            {
                octal2++;
            } else if(octaltarget == 1)
            {
                octal1++;
            }

            if(autoincrementafter)
            {
                if (octal4 >= 9)
                {
                    if (!(octal4 > 9))
                    {
                        octal4 = 0;
                    }
                    if (octal3 == 9)
                    {
                        if (!(octal3 > 9))
                        {
                            octal3 = 0;
                        }
                        if (octal2 == 9)
                        {
                            if (!(octal2 > 9))
                            {
                                octal2 = 0;
                            }
                            if (octal1 == 9)
                            {
                                octal1 = octal1 + 0;
                            }
                            else
                            {
                                octal1++;
                            }
                        }
                        else
                        {
                            octal2++;
                        }
                    }
                    else
                    {
                        octal3++;
                    }
                }
                else
                {
                    octal4++;
                }
            }

            return octal1 + "." + octal2 + "." + octal3 + "." + octal4;
        }

    }
}
