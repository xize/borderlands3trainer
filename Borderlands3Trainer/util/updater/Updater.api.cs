﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gitlabupdater
{
    public partial class Updater
    {

        public String GetFullRepository()
        {
            return this.repo;
        }

        public String GetRepository()
        {
            return this.cleanrepo;
        }

        public String GetCurrentVersion()
        {
            return this.currentversion;
        }

        public OctalCalculator GetOctalCalculator()
        {
            return this.calc;
        }

        public String LatestURL
        {
            get
            {
                if (this.latesturl == null || this.latesturl == "")
                {
                    throw new NullReferenceException("latest url is empty!");
                }
                return this.latesturl;
            }
            set
            {
                this.latesturl = value;
            }
        }

        public String DownloadString
        {

            set
            {
                this.downloadlink = value;
            }
            get
            {
                if (this.downloadlink == null || this.downloadlink == "")
                {
                    throw new NullReferenceException("download link is empty!");
                }
                return this.downloadlink;
            }
        }

        public String DownloadedCheckSum
        {

            set
            {
                this.downloadedcchecksum = value;
            }
            get
            {
                return this.downloadedcchecksum;
            }
        }

        public String CleanURL
        {
            get
            {
                return this.cleanrepo;
            }
        }

        public String NewVersion
        {
            get
            {
                return this.newversion;
            }
            set
            {
                this.newversion = value;
            }
        }

        public String[] Changelog
        {
            get
            {
                return this.changelog;
            }
            set
            {
                this.changelog = value;
            }
        }
    }
}
