﻿using Borderlands3Trainer.cheats;
using System;

namespace Borderlands3Trainer
{
    partial class Window
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Window));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tooltip = new System.Windows.Forms.ToolTip(this.components);
            this.teleportbox = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.version = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cursorholder = new System.Windows.Forms.Panel();
            this.centerpanel = new System.Windows.Forms.Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.infammocheckbox = new Borderlands3Trainer.controls.CustomCheckBox();
            this.grenadecheckbox = new Borderlands3Trainer.controls.CustomCheckBox();
            this.shieldcheckbox = new Borderlands3Trainer.controls.CustomCheckBox();
            this.godmodecheckbox = new Borderlands3Trainer.controls.CustomCheckBox();
            this.goldkeybtn = new System.Windows.Forms.Button();
            this.eridiumbtn = new System.Windows.Forms.Button();
            this.moneybtn = new System.Windows.Forms.Button();
            this.teleportsetbtn = new System.Windows.Forms.Button();
            this.mute = new System.Windows.Forms.CheckBox();
            this.tpundo = new System.Windows.Forms.Button();
            this.disablehotkeyscheckbox = new System.Windows.Forms.CheckBox();
            this.goldkeynumeric = new System.Windows.Forms.NumericUpDown();
            this.label16 = new System.Windows.Forms.Label();
            this.eridiumnumeric = new System.Windows.Forms.NumericUpDown();
            this.eridiumlabel = new System.Windows.Forms.Label();
            this.moneynumeric = new System.Windows.Forms.NumericUpDown();
            this.moneylabel = new System.Windows.Forms.Label();
            this.tpbutton = new System.Windows.Forms.Button();
            this.teleportlabel = new System.Windows.Forms.Label();
            this.hookedstatus = new System.Windows.Forms.Label();
            this.norecoillabel = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.infintivegrenadeslabel = new System.Windows.Forms.Label();
            this.infinitiveshieldlabel = new System.Windows.Forms.Label();
            this.godmodelabel = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.playerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.worldToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.roundedwindow = new Borderlands3Trainer.controls.RoundedObject();
            this.fadeControl1 = new Borderlands3Trainer.controls.FadeControl();
            this.moveAbleWindow1 = new Borderlands3Trainer.controls.MoveAbleWindow();
            this.panel2.SuspendLayout();
            this.cursorholder.SuspendLayout();
            this.centerpanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.goldkeynumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eridiumnumeric)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneynumeric)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.label1.Location = new System.Drawing.Point(365, 9);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(5);
            this.label1.Size = new System.Drawing.Size(25, 23);
            this.label1.TabIndex = 0;
            this.label1.Text = "X";
            this.label1.Click += new System.EventHandler(this.Label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.label2.Location = new System.Drawing.Point(343, 9);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(5);
            this.label2.Size = new System.Drawing.Size(24, 23);
            this.label2.TabIndex = 1;
            this.label2.Text = "_";
            this.label2.Click += new System.EventHandler(this.Label2_Click);
            // 
            // teleportbox
            // 
            this.teleportbox.BackColor = System.Drawing.Color.Black;
            this.teleportbox.DropDownWidth = 800;
            this.teleportbox.Enabled = false;
            this.teleportbox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.teleportbox.ForeColor = System.Drawing.Color.White;
            this.teleportbox.FormattingEnabled = true;
            this.teleportbox.Location = new System.Drawing.Point(220, 117);
            this.teleportbox.Name = "teleportbox";
            this.teleportbox.Size = new System.Drawing.Size(57, 21);
            this.teleportbox.TabIndex = 62;
            this.tooltip.SetToolTip(this.teleportbox, "use < > keys to switch teleport");
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.White;
            this.label6.Location = new System.Drawing.Point(353, 2);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 13);
            this.label6.TabIndex = 43;
            this.label6.Text = "credits";
            this.label6.Click += new System.EventHandler(this.Label6_Click);
            // 
            // version
            // 
            this.version.AutoSize = true;
            this.version.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.version.Dock = System.Windows.Forms.DockStyle.Left;
            this.version.ForeColor = System.Drawing.Color.White;
            this.version.Location = new System.Drawing.Point(0, 0);
            this.version.Name = "version";
            this.version.Padding = new System.Windows.Forms.Padding(5);
            this.version.Size = new System.Drawing.Size(49, 23);
            this.version.TabIndex = 45;
            this.version.Text = "ver {0}";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Transparent;
            this.panel2.Controls.Add(this.version);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 357);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(400, 23);
            this.panel2.TabIndex = 46;
            // 
            // cursorholder
            // 
            this.cursorholder.BackColor = System.Drawing.Color.Transparent;
            this.cursorholder.BackgroundImage = global::Borderlands3Trainer.Properties.Resources.header;
            this.cursorholder.Controls.Add(this.label1);
            this.cursorholder.Controls.Add(this.label2);
            this.cursorholder.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cursorholder.Dock = System.Windows.Forms.DockStyle.Top;
            this.cursorholder.Location = new System.Drawing.Point(0, 0);
            this.cursorholder.Name = "cursorholder";
            this.cursorholder.Size = new System.Drawing.Size(400, 52);
            this.cursorholder.TabIndex = 47;
            // 
            // centerpanel
            // 
            this.centerpanel.BackColor = System.Drawing.Color.Black;
            this.centerpanel.BackgroundImage = global::Borderlands3Trainer.Properties.Resources.center2;
            this.centerpanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.centerpanel.Controls.Add(this.label5);
            this.centerpanel.Controls.Add(this.infammocheckbox);
            this.centerpanel.Controls.Add(this.grenadecheckbox);
            this.centerpanel.Controls.Add(this.shieldcheckbox);
            this.centerpanel.Controls.Add(this.godmodecheckbox);
            this.centerpanel.Controls.Add(this.goldkeybtn);
            this.centerpanel.Controls.Add(this.eridiumbtn);
            this.centerpanel.Controls.Add(this.moneybtn);
            this.centerpanel.Controls.Add(this.teleportsetbtn);
            this.centerpanel.Controls.Add(this.mute);
            this.centerpanel.Controls.Add(this.tpundo);
            this.centerpanel.Controls.Add(this.disablehotkeyscheckbox);
            this.centerpanel.Controls.Add(this.goldkeynumeric);
            this.centerpanel.Controls.Add(this.label16);
            this.centerpanel.Controls.Add(this.eridiumnumeric);
            this.centerpanel.Controls.Add(this.eridiumlabel);
            this.centerpanel.Controls.Add(this.moneynumeric);
            this.centerpanel.Controls.Add(this.moneylabel);
            this.centerpanel.Controls.Add(this.tpbutton);
            this.centerpanel.Controls.Add(this.teleportbox);
            this.centerpanel.Controls.Add(this.teleportlabel);
            this.centerpanel.Controls.Add(this.hookedstatus);
            this.centerpanel.Controls.Add(this.norecoillabel);
            this.centerpanel.Controls.Add(this.label10);
            this.centerpanel.Controls.Add(this.label8);
            this.centerpanel.Controls.Add(this.label9);
            this.centerpanel.Controls.Add(this.infintivegrenadeslabel);
            this.centerpanel.Controls.Add(this.infinitiveshieldlabel);
            this.centerpanel.Controls.Add(this.godmodelabel);
            this.centerpanel.Controls.Add(this.label4);
            this.centerpanel.Controls.Add(this.label3);
            this.centerpanel.Controls.Add(this.panel1);
            this.centerpanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.centerpanel.Location = new System.Drawing.Point(0, 52);
            this.centerpanel.Name = "centerpanel";
            this.centerpanel.Size = new System.Drawing.Size(400, 299);
            this.centerpanel.TabIndex = 50;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.ForeColor = System.Drawing.Color.Green;
            this.label5.Location = new System.Drawing.Point(8, 193);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(89, 13);
            this.label5.TabIndex = 105;
            this.label5.Text = "Borderlands3.exe";
            // 
            // infammocheckbox
            // 
            this.infammocheckbox.BackColor = System.Drawing.Color.Transparent;
            this.infammocheckbox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("infammocheckbox.BackgroundImage")));
            this.infammocheckbox.Checked = false;
            this.infammocheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.infammocheckbox.Enabled = false;
            this.infammocheckbox.Location = new System.Drawing.Point(354, 100);
            this.infammocheckbox.MaximumSize = new System.Drawing.Size(23, 12);
            this.infammocheckbox.MinimumSize = new System.Drawing.Size(23, 12);
            this.infammocheckbox.Name = "infammocheckbox";
            this.infammocheckbox.Size = new System.Drawing.Size(23, 12);
            this.infammocheckbox.TabIndex = 104;
            // 
            // grenadecheckbox
            // 
            this.grenadecheckbox.BackColor = System.Drawing.Color.Transparent;
            this.grenadecheckbox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("grenadecheckbox.BackgroundImage")));
            this.grenadecheckbox.Checked = false;
            this.grenadecheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.grenadecheckbox.Location = new System.Drawing.Point(354, 80);
            this.grenadecheckbox.MaximumSize = new System.Drawing.Size(23, 12);
            this.grenadecheckbox.MinimumSize = new System.Drawing.Size(23, 12);
            this.grenadecheckbox.Name = "grenadecheckbox";
            this.grenadecheckbox.Size = new System.Drawing.Size(23, 12);
            this.grenadecheckbox.TabIndex = 103;
            this.grenadecheckbox.CustomCheckBoxChange += new System.EventHandler<Borderlands3Trainer.cheats.CustomCheckBoxEventArgs>(this.grenadecheckbox_CustomCheckBoxChange);
            // 
            // shieldcheckbox
            // 
            this.shieldcheckbox.BackColor = System.Drawing.Color.Transparent;
            this.shieldcheckbox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("shieldcheckbox.BackgroundImage")));
            this.shieldcheckbox.Checked = false;
            this.shieldcheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.shieldcheckbox.Location = new System.Drawing.Point(354, 59);
            this.shieldcheckbox.MaximumSize = new System.Drawing.Size(23, 12);
            this.shieldcheckbox.MinimumSize = new System.Drawing.Size(23, 12);
            this.shieldcheckbox.Name = "shieldcheckbox";
            this.shieldcheckbox.Size = new System.Drawing.Size(23, 12);
            this.shieldcheckbox.TabIndex = 102;
            this.shieldcheckbox.CustomCheckBoxChange += new System.EventHandler<Borderlands3Trainer.cheats.CustomCheckBoxEventArgs>(this.shieldcheckbox_CustomCheckBoxChange);
            // 
            // godmodecheckbox
            // 
            this.godmodecheckbox.BackColor = System.Drawing.Color.Transparent;
            this.godmodecheckbox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("godmodecheckbox.BackgroundImage")));
            this.godmodecheckbox.Checked = false;
            this.godmodecheckbox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.godmodecheckbox.Location = new System.Drawing.Point(354, 39);
            this.godmodecheckbox.MaximumSize = new System.Drawing.Size(23, 12);
            this.godmodecheckbox.MinimumSize = new System.Drawing.Size(23, 12);
            this.godmodecheckbox.Name = "godmodecheckbox";
            this.godmodecheckbox.Size = new System.Drawing.Size(23, 12);
            this.godmodecheckbox.TabIndex = 101;
            this.godmodecheckbox.CustomCheckBoxChange += new System.EventHandler<Borderlands3Trainer.cheats.CustomCheckBoxEventArgs>(this.godmodecheckbox_CustomCheckBoxChange);
            // 
            // goldkeybtn
            // 
            this.goldkeybtn.AutoSize = true;
            this.goldkeybtn.BackColor = System.Drawing.Color.Black;
            this.goldkeybtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.goldkeybtn.Enabled = false;
            this.goldkeybtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.goldkeybtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.goldkeybtn.ForeColor = System.Drawing.Color.White;
            this.goldkeybtn.Location = new System.Drawing.Point(333, 210);
            this.goldkeybtn.Name = "goldkeybtn";
            this.goldkeybtn.Size = new System.Drawing.Size(44, 25);
            this.goldkeybtn.TabIndex = 94;
            this.goldkeybtn.Text = "give";
            this.goldkeybtn.UseVisualStyleBackColor = false;
            this.goldkeybtn.Click += new System.EventHandler(this.Goldkeybtn_Click);
            // 
            // eridiumbtn
            // 
            this.eridiumbtn.AutoSize = true;
            this.eridiumbtn.BackColor = System.Drawing.Color.Black;
            this.eridiumbtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.eridiumbtn.Enabled = false;
            this.eridiumbtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.eridiumbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.eridiumbtn.ForeColor = System.Drawing.Color.White;
            this.eridiumbtn.Location = new System.Drawing.Point(333, 179);
            this.eridiumbtn.Name = "eridiumbtn";
            this.eridiumbtn.Size = new System.Drawing.Size(44, 25);
            this.eridiumbtn.TabIndex = 93;
            this.eridiumbtn.Text = "give";
            this.eridiumbtn.UseVisualStyleBackColor = false;
            this.eridiumbtn.Click += new System.EventHandler(this.Eridiumbtn_Click);
            // 
            // moneybtn
            // 
            this.moneybtn.AutoSize = true;
            this.moneybtn.BackColor = System.Drawing.Color.Black;
            this.moneybtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.moneybtn.Enabled = false;
            this.moneybtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.moneybtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.moneybtn.ForeColor = System.Drawing.Color.White;
            this.moneybtn.Location = new System.Drawing.Point(333, 146);
            this.moneybtn.Name = "moneybtn";
            this.moneybtn.Size = new System.Drawing.Size(44, 25);
            this.moneybtn.TabIndex = 92;
            this.moneybtn.Text = "give";
            this.moneybtn.UseVisualStyleBackColor = false;
            this.moneybtn.Click += new System.EventHandler(this.Moneybtn_Click);
            // 
            // teleportsetbtn
            // 
            this.teleportsetbtn.BackColor = System.Drawing.Color.Black;
            this.teleportsetbtn.Cursor = System.Windows.Forms.Cursors.Hand;
            this.teleportsetbtn.Enabled = false;
            this.teleportsetbtn.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.teleportsetbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.teleportsetbtn.ForeColor = System.Drawing.Color.White;
            this.teleportsetbtn.Location = new System.Drawing.Point(282, 117);
            this.teleportsetbtn.Name = "teleportsetbtn";
            this.teleportsetbtn.Size = new System.Drawing.Size(34, 23);
            this.teleportsetbtn.TabIndex = 85;
            this.teleportsetbtn.Text = "edit";
            this.teleportsetbtn.UseVisualStyleBackColor = false;
            this.teleportsetbtn.Click += new System.EventHandler(this.Teleportsetbtn_Click);
            // 
            // mute
            // 
            this.mute.AutoSize = true;
            this.mute.BackColor = System.Drawing.Color.Transparent;
            this.mute.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.mute.Location = new System.Drawing.Point(11, 251);
            this.mute.Name = "mute";
            this.mute.Size = new System.Drawing.Size(49, 17);
            this.mute.TabIndex = 80;
            this.mute.Text = "mute";
            this.mute.UseVisualStyleBackColor = false;
            // 
            // tpundo
            // 
            this.tpundo.BackColor = System.Drawing.Color.Transparent;
            this.tpundo.BackgroundImage = global::Borderlands3Trainer.Properties.Resources.Undo_icon31;
            this.tpundo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tpundo.Enabled = false;
            this.tpundo.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.tpundo.FlatAppearance.BorderSize = 0;
            this.tpundo.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkGray;
            this.tpundo.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.tpundo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tpundo.ForeColor = System.Drawing.Color.White;
            this.tpundo.Location = new System.Drawing.Point(359, 120);
            this.tpundo.Name = "tpundo";
            this.tpundo.Size = new System.Drawing.Size(16, 16);
            this.tpundo.TabIndex = 79;
            this.tpundo.UseVisualStyleBackColor = false;
            this.tpundo.Click += new System.EventHandler(this.Tpundo_Click);
            // 
            // disablehotkeyscheckbox
            // 
            this.disablehotkeyscheckbox.AutoSize = true;
            this.disablehotkeyscheckbox.BackColor = System.Drawing.Color.Transparent;
            this.disablehotkeyscheckbox.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.disablehotkeyscheckbox.Location = new System.Drawing.Point(11, 227);
            this.disablehotkeyscheckbox.Name = "disablehotkeyscheckbox";
            this.disablehotkeyscheckbox.Size = new System.Drawing.Size(105, 17);
            this.disablehotkeyscheckbox.TabIndex = 78;
            this.disablehotkeyscheckbox.Text = "disable hotkeys?";
            this.disablehotkeyscheckbox.UseVisualStyleBackColor = false;
            // 
            // goldkeynumeric
            // 
            this.goldkeynumeric.BackColor = System.Drawing.Color.Black;
            this.goldkeynumeric.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.goldkeynumeric.Enabled = false;
            this.goldkeynumeric.ForeColor = System.Drawing.Color.White;
            this.goldkeynumeric.Location = new System.Drawing.Point(270, 211);
            this.goldkeynumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.goldkeynumeric.Name = "goldkeynumeric";
            this.goldkeynumeric.Size = new System.Drawing.Size(57, 20);
            this.goldkeynumeric.TabIndex = 69;
            this.goldkeynumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.BackColor = System.Drawing.Color.Transparent;
            this.label16.Enabled = false;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.label16.Location = new System.Drawing.Point(128, 216);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(125, 13);
            this.label16.TabIndex = 68;
            this.label16.Text = "num 7 give golden keys: ";
            // 
            // eridiumnumeric
            // 
            this.eridiumnumeric.BackColor = System.Drawing.Color.Black;
            this.eridiumnumeric.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.eridiumnumeric.Enabled = false;
            this.eridiumnumeric.ForeColor = System.Drawing.Color.White;
            this.eridiumnumeric.Location = new System.Drawing.Point(270, 180);
            this.eridiumnumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.eridiumnumeric.Name = "eridiumnumeric";
            this.eridiumnumeric.Size = new System.Drawing.Size(57, 20);
            this.eridiumnumeric.TabIndex = 67;
            this.eridiumnumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // eridiumlabel
            // 
            this.eridiumlabel.AutoSize = true;
            this.eridiumlabel.BackColor = System.Drawing.Color.Transparent;
            this.eridiumlabel.Enabled = false;
            this.eridiumlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.eridiumlabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.eridiumlabel.Location = new System.Drawing.Point(129, 184);
            this.eridiumlabel.Name = "eridiumlabel";
            this.eridiumlabel.Size = new System.Drawing.Size(102, 13);
            this.eridiumlabel.TabIndex = 66;
            this.eridiumlabel.Text = "num 6 give Eridium: ";
            // 
            // moneynumeric
            // 
            this.moneynumeric.BackColor = System.Drawing.Color.Black;
            this.moneynumeric.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.moneynumeric.Enabled = false;
            this.moneynumeric.ForeColor = System.Drawing.Color.White;
            this.moneynumeric.Location = new System.Drawing.Point(270, 149);
            this.moneynumeric.Maximum = new decimal(new int[] {
            800000000,
            0,
            0,
            0});
            this.moneynumeric.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.moneynumeric.Name = "moneynumeric";
            this.moneynumeric.Size = new System.Drawing.Size(57, 20);
            this.moneynumeric.TabIndex = 65;
            this.moneynumeric.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // moneylabel
            // 
            this.moneylabel.AutoSize = true;
            this.moneylabel.BackColor = System.Drawing.Color.Transparent;
            this.moneylabel.Enabled = false;
            this.moneylabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.moneylabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.moneylabel.Location = new System.Drawing.Point(129, 151);
            this.moneylabel.Name = "moneylabel";
            this.moneylabel.Size = new System.Drawing.Size(99, 13);
            this.moneylabel.TabIndex = 64;
            this.moneylabel.Text = "num 5 give money: ";
            // 
            // tpbutton
            // 
            this.tpbutton.BackColor = System.Drawing.Color.Black;
            this.tpbutton.Cursor = System.Windows.Forms.Cursors.Hand;
            this.tpbutton.Enabled = false;
            this.tpbutton.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.tpbutton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.tpbutton.ForeColor = System.Drawing.Color.White;
            this.tpbutton.Location = new System.Drawing.Point(320, 117);
            this.tpbutton.Name = "tpbutton";
            this.tpbutton.Size = new System.Drawing.Size(30, 23);
            this.tpbutton.TabIndex = 63;
            this.tpbutton.Text = "tp";
            this.tpbutton.UseVisualStyleBackColor = false;
            this.tpbutton.Click += new System.EventHandler(this.tpbutton_Click);
            // 
            // teleportlabel
            // 
            this.teleportlabel.AutoSize = true;
            this.teleportlabel.BackColor = System.Drawing.Color.Transparent;
            this.teleportlabel.Enabled = false;
            this.teleportlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.teleportlabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.teleportlabel.Location = new System.Drawing.Point(129, 121);
            this.teleportlabel.Name = "teleportlabel";
            this.teleportlabel.Size = new System.Drawing.Size(80, 13);
            this.teleportlabel.TabIndex = 61;
            this.teleportlabel.Text = "num 4 teleport: ";
            // 
            // hookedstatus
            // 
            this.hookedstatus.AutoSize = true;
            this.hookedstatus.BackColor = System.Drawing.Color.Transparent;
            this.hookedstatus.ForeColor = System.Drawing.Color.Green;
            this.hookedstatus.Location = new System.Drawing.Point(52, 206);
            this.hookedstatus.Name = "hookedstatus";
            this.hookedstatus.Size = new System.Drawing.Size(25, 13);
            this.hookedstatus.TabIndex = 60;
            this.hookedstatus.Text = "true";
            // 
            // norecoillabel
            // 
            this.norecoillabel.AutoSize = true;
            this.norecoillabel.BackColor = System.Drawing.Color.Transparent;
            this.norecoillabel.Enabled = false;
            this.norecoillabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.norecoillabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.norecoillabel.Location = new System.Drawing.Point(129, 99);
            this.norecoillabel.Name = "norecoillabel";
            this.norecoillabel.Size = new System.Drawing.Size(84, 13);
            this.norecoillabel.TabIndex = 59;
            this.norecoillabel.Text = "num 3: inf ammo";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.label10.Location = new System.Drawing.Point(8, 206);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 13);
            this.label10.TabIndex = 58;
            this.label10.Text = "Hooked:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label8.Location = new System.Drawing.Point(12, 214);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(0, 13);
            this.label8.TabIndex = 57;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.label9.Location = new System.Drawing.Point(9, 180);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(77, 13);
            this.label9.TabIndex = 56;
            this.label9.Text = "target process:";
            // 
            // infintivegrenadeslabel
            // 
            this.infintivegrenadeslabel.AutoSize = true;
            this.infintivegrenadeslabel.BackColor = System.Drawing.Color.Transparent;
            this.infintivegrenadeslabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.infintivegrenadeslabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.infintivegrenadeslabel.Location = new System.Drawing.Point(129, 79);
            this.infintivegrenadeslabel.Name = "infintivegrenadeslabel";
            this.infintivegrenadeslabel.Size = new System.Drawing.Size(127, 13);
            this.infintivegrenadeslabel.TabIndex = 55;
            this.infintivegrenadeslabel.Text = "num 2: infinitive grenades";
            // 
            // infinitiveshieldlabel
            // 
            this.infinitiveshieldlabel.AutoSize = true;
            this.infinitiveshieldlabel.BackColor = System.Drawing.Color.Transparent;
            this.infinitiveshieldlabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.infinitiveshieldlabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.infinitiveshieldlabel.Location = new System.Drawing.Point(129, 58);
            this.infinitiveshieldlabel.Name = "infinitiveshieldlabel";
            this.infinitiveshieldlabel.Size = new System.Drawing.Size(110, 13);
            this.infinitiveshieldlabel.TabIndex = 54;
            this.infinitiveshieldlabel.Text = "num 1: infinitive shield";
            // 
            // godmodelabel
            // 
            this.godmodelabel.AutoSize = true;
            this.godmodelabel.BackColor = System.Drawing.Color.Transparent;
            this.godmodelabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.godmodelabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(201)))), ((int)(((byte)(201)))));
            this.godmodelabel.Location = new System.Drawing.Point(129, 38);
            this.godmodelabel.Name = "godmodelabel";
            this.godmodelabel.Size = new System.Drawing.Size(86, 13);
            this.godmodelabel.TabIndex = 53;
            this.godmodelabel.Text = "num 0: godmode";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.label4.Location = new System.Drawing.Point(128, 14);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(50, 13);
            this.label4.TabIndex = 52;
            this.label4.Text = "Cheats:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(224)))));
            this.label3.Location = new System.Drawing.Point(12, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 13);
            this.label3.TabIndex = 51;
            this.label3.Text = "Game:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Black;
            this.panel1.BackgroundImage = global::Borderlands3Trainer.Properties.Resources.borderlands_3_pc_buy_now_cd_key;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(12, 38);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(107, 137);
            this.panel1.TabIndex = 50;
            // 
            // playerToolStripMenuItem
            // 
            this.playerToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.playerToolStripMenuItem.Name = "playerToolStripMenuItem";
            this.playerToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.playerToolStripMenuItem.Text = "Player";
            // 
            // worldToolStripMenuItem
            // 
            this.worldToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.worldToolStripMenuItem.Name = "worldToolStripMenuItem";
            this.worldToolStripMenuItem.Size = new System.Drawing.Size(51, 20);
            this.worldToolStripMenuItem.Text = "World";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // testToolStripMenuItem
            // 
            this.testToolStripMenuItem.BackColor = System.Drawing.Color.Black;
            this.testToolStripMenuItem.ForeColor = System.Drawing.Color.White;
            this.testToolStripMenuItem.Name = "testToolStripMenuItem";
            this.testToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.testToolStripMenuItem.Text = "test";
            // 
            // roundedwindow
            // 
            this.roundedwindow.Border = 0;
            this.roundedwindow.BorderColor = System.Drawing.Color.Transparent;
            this.roundedwindow.Radius = 8;
            this.roundedwindow.TargetControl = this;
            // 
            // fadeControl1
            // 
            this.fadeControl1.Target = this;
            this.fadeControl1.FadeFinish += new System.EventHandler(this.fadeControl1_FadeFinish);
            // 
            // moveAbleWindow1
            // 
            this.moveAbleWindow1.ControlTarget = null;
            this.moveAbleWindow1.MainWindowTarget = this;
            // 
            // Window
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.Black;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(400, 380);
            this.Controls.Add(this.centerpanel);
            this.Controls.Add(this.cursorholder);
            this.Controls.Add(this.panel2);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(400, 380);
            this.Name = "Window";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Borderlands 3 Trainer";
            this.TransparencyKey = System.Drawing.Color.Orange;
            this.Load += new System.EventHandler(this.Window2_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.cursorholder.ResumeLayout(false);
            this.cursorholder.PerformLayout();
            this.centerpanel.ResumeLayout(false);
            this.centerpanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.goldkeynumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eridiumnumeric)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.moneynumeric)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolTip tooltip;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label version;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel cursorholder;
        private System.Windows.Forms.Panel centerpanel;
        public System.Windows.Forms.CheckBox mute;
        private System.Windows.Forms.CheckBox disablehotkeyscheckbox;
        public System.Windows.Forms.Label hookedstatus;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ToolStripMenuItem playerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem worldToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        public controls.CustomCheckBox infammocheckbox;
        public controls.CustomCheckBox grenadecheckbox;
        public controls.CustomCheckBox shieldcheckbox;
        public controls.CustomCheckBox godmodecheckbox;
        private System.Windows.Forms.Button goldkeybtn;
        private System.Windows.Forms.Button eridiumbtn;
        private System.Windows.Forms.Button moneybtn;
        private System.Windows.Forms.Button teleportsetbtn;
        private System.Windows.Forms.Button tpundo;
        public System.Windows.Forms.NumericUpDown goldkeynumeric;
        private System.Windows.Forms.Label label16;
        public System.Windows.Forms.NumericUpDown eridiumnumeric;
        private System.Windows.Forms.Label eridiumlabel;
        public System.Windows.Forms.NumericUpDown moneynumeric;
        private System.Windows.Forms.Label moneylabel;
        private System.Windows.Forms.Button tpbutton;
        private System.Windows.Forms.ComboBox teleportbox;
        private System.Windows.Forms.Label teleportlabel;
        private System.Windows.Forms.Label norecoillabel;
        private System.Windows.Forms.Label infintivegrenadeslabel;
        private System.Windows.Forms.Label infinitiveshieldlabel;
        private System.Windows.Forms.Label godmodelabel;
        private System.Windows.Forms.Label label5;
        private controls.RoundedObject roundedwindow;
        private controls.FadeControl fadeControl1;
        private controls.MoveAbleWindow moveAbleWindow1;
    }
}