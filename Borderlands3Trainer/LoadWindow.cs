﻿using Borderlands3Trainer.util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands3Trainer
{
    public partial class LoadWindow : Form
    {

        private Window win;
        private ARTWindow art;
        private Random rand = new Random();

        public LoadWindow()
        {
            InitializeComponent();
        }

        private void LoadWindow_Load(object sender, EventArgs e)
        {
            this.art = new ARTWindow();
            art.Show();
            art.GotFocus += (t, s) =>
            {
                this.Focus();
            };

            this.BringToFront();
            this.Hide();
            this.win = new Window();

            this.version.Text = String.Format(this.version.Text, Application.ProductVersion);

            fadeControl1.Start();
        }

        private void fadeControl1_FadeFinish(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            this.worker.RunWorkerAsync();
        }

        private int process = 0;

        private void Work(object sender, DoWorkEventArgs e)
        {
            Task.Delay(501).Wait();
            this.worker.ReportProgress(30);
            Task.Delay(2101).Wait();
            this.worker.ReportProgress(80);
            Task.Delay(900);
            this.worker.ReportProgress(100);
            Task.Delay(1200);
        }

        private void Progress(object sender, ProgressChangedEventArgs e)
        {
            if(e.ProgressPercentage >= 100)
            {
                progressbar.Value = 100;
                this.worker.CancelAsync();
            }
            this.progressbar.Value = e.ProgressPercentage;
            this.progresslabel.Text = e.ProgressPercentage+"%";
            this.progressbar.Update();
        }

        private void Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            this.art.Close();

            this.Hide();
            this.win.BringToFront();
            this.win.Show();
        }
    }
}
