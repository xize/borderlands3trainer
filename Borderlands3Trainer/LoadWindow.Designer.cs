﻿using Borderlands3Trainer.controls;

namespace Borderlands3Trainer
{
    partial class LoadWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoadWindow));
            this.loadprogress = new System.Windows.Forms.Label();
            this.version = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.progresslabel = new System.Windows.Forms.Label();
            this.progressbar = new Borderlands3Trainer.controls.CustomProgressbar();
            this.roundedObject1 = new Borderlands3Trainer.controls.RoundedObject();
            this.fadeControl1 = new Borderlands3Trainer.controls.FadeControl();
            this.roundedObject2 = new Borderlands3Trainer.controls.RoundedObject();
            this.worker = new Borderlands3Trainer.controls.AbortableBackgroundWorker();
            this.SuspendLayout();
            // 
            // loadprogress
            // 
            this.loadprogress.AutoSize = true;
            this.loadprogress.BackColor = System.Drawing.Color.Transparent;
            this.loadprogress.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.loadprogress.ForeColor = System.Drawing.Color.LightBlue;
            this.loadprogress.Location = new System.Drawing.Point(81, 201);
            this.loadprogress.Name = "loadprogress";
            this.loadprogress.Size = new System.Drawing.Size(42, 17);
            this.loadprogress.TabIndex = 3;
            this.loadprogress.Text = "{0}%";
            // 
            // version
            // 
            this.version.AutoSize = true;
            this.version.BackColor = System.Drawing.Color.Transparent;
            this.version.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.version.Location = new System.Drawing.Point(288, 9);
            this.version.Name = "version";
            this.version.Size = new System.Drawing.Size(62, 13);
            this.version.TabIndex = 6;
            this.version.Text = "Version: {0}";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label1.Location = new System.Drawing.Point(301, 160);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(82, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "starting trainer...";
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(291, 25);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(95, 1);
            this.panel1.TabIndex = 9;
            // 
            // progresslabel
            // 
            this.progresslabel.AutoSize = true;
            this.progresslabel.BackColor = System.Drawing.Color.Transparent;
            this.progresslabel.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.progresslabel.Location = new System.Drawing.Point(567, 160);
            this.progresslabel.Name = "progresslabel";
            this.progresslabel.Size = new System.Drawing.Size(21, 13);
            this.progresslabel.TabIndex = 11;
            this.progresslabel.Text = "0%";
            // 
            // progressbar
            // 
            this.progressbar.BackgroundColor = System.Drawing.Color.DarkGray;
            this.progressbar.CornerRadius = 3;
            this.progressbar.Location = new System.Drawing.Point(304, 176);
            this.progressbar.Name = "progressbar";
            this.progressbar.Size = new System.Drawing.Size(284, 13);
            this.progressbar.TabIndex = 8;
            // 
            // roundedObject1
            // 
            this.roundedObject1.Border = 8;
            this.roundedObject1.BorderColor = System.Drawing.Color.Gainsboro;
            this.roundedObject1.Radius = 8;
            this.roundedObject1.TargetControl = this;
            // 
            // fadeControl1
            // 
            this.fadeControl1.Target = this;
            this.fadeControl1.FadeFinish += new System.EventHandler(this.fadeControl1_FadeFinish);
            // 
            // roundedObject2
            // 
            this.roundedObject2.Border = 0;
            this.roundedObject2.BorderColor = System.Drawing.Color.Transparent;
            this.roundedObject2.Radius = 8;
            // 
            // worker
            // 
            this.worker.WorkerReportsProgress = true;
            this.worker.WorkerSupportsCancellation = true;
            this.worker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.Work);
            this.worker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.Progress);
            this.worker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.Completed);
            // 
            // LoadWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Borderlands3Trainer.Properties.Resources.ide4;
            this.ClientSize = new System.Drawing.Size(600, 200);
            this.Controls.Add(this.progresslabel);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.progressbar);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.version);
            this.Controls.Add(this.loadprogress);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "LoadWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "LoadWindow";
            this.Load += new System.EventHandler(this.LoadWindow_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label loadprogress;
        private System.Windows.Forms.Label version;
        private controls.RoundedObject roundedObject1;
        private System.Windows.Forms.Label label1;
        private controls.CustomProgressbar progressbar;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label progresslabel;
        private controls.FadeControl fadeControl1;
        private controls.RoundedObject roundedObject2;
        private AbortableBackgroundWorker worker;
    }
}