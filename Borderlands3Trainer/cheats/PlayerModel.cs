﻿using Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands3Trainer.cheats
{
    public class PlayerModel : ModelCheat, IPlayer
    {

        private Location lastLocation;


        public PlayerModel(Mem m) : base(m)
        {

        }

        public String GodmodeAddress
        {
            get; set;
        }

        public bool IsGodmodeAddress()
        {
            if (this.GodmodeAddress != null)
            {
                return true;
            }
            return false;
        }


        private float maxhealth = 0;
        public void DoGodmode()
        {
            if (!this.IsGodmodeAddress())
            {
                throw new System.Exception("tried to activate godmode cheat when no base address has been set!");
            }

            float health = this.GetMemory().ReadFloat(GodmodeAddress, "");
            if (health < this.maxhealth)
            {
                health = this.maxhealth;
            } else if (health > this.maxhealth)
            {
                this.maxhealth = health;
            }

            this.GetMemory().WriteMemory(GodmodeAddress, "float", this.maxhealth + "");
        }

        public String InfinitiveShieldAddress
        {
            get; set;
        }

        public bool IsInfinitiveShieldAddress()
        {
            if (InfinitiveShieldAddress != null)
            {
                return true;
            }
            return false;
        }


        private float maxshield = 0;
        public void DoInfinitiveShield()
        {
            if (!IsInfinitiveShieldAddress())
            {
                throw new System.Exception("tried to activate shield cheat when no base address has been set!");
            }
            float shield = this.GetMemory().ReadFloat(InfinitiveShieldAddress, "");
            if (shield > this.maxshield)
            {
                this.maxshield = shield;
            } else if (shield < this.maxshield)
            {
                shield = this.maxshield;
            }

            this.GetMemory().WriteMemory(InfinitiveShieldAddress, "float", this.maxshield + "");
        }

        public String InfinitiveGrenadeAddress
        {
            get; set;
        }

        public bool IsInfinitiveGrenadeAddress()
        {
            if (InfinitiveGrenadeAddress != null)
            {
                return true;
            }
            return false;
        }


        private float maxgrenade = 0;
        public void DoInfinitiveGrenades()
        {
            if (!IsInfinitiveGrenadeAddress())
            {
                throw new System.Exception("tried to activate infinitive grenade cheat when no base address has been set!");
            }

            float grenades = this.GetMemory().ReadFloat(InfinitiveGrenadeAddress, "", false);
            if (grenades > this.maxgrenade)
            {
                this.maxgrenade = grenades;
            } else if (grenades < this.maxgrenade)
            {
                grenades = this.maxgrenade;
            }

            this.GetMemory().WriteMemory(InfinitiveGrenadeAddress, "float", this.maxgrenade + "");
        }

        public String[] InfinitiveAmmoAddresses {
            get;set;
        }

        public bool IsInfinitiveAmmoAddresses()
        {
            if(InfinitiveAmmoAddresses != null)
            {
                return true;
            }
            return false;
        }

        private float pammo = 0;
        private float sammo = 0;
        private float mammo = 0;
        private float shammo = 0;
        private float rammo = 0;
        private float snammo = 0;

        public void DoInfinitiveAmmo()
        {
            if (!IsInfinitiveAmmoAddresses())
            {
                throw new System.Exception("tried to activate infinitive ammo cheat when no base address has been set!");
            }

            String pistoladdr = InfinitiveAmmoAddresses[0];
            String smgaddr = InfinitiveAmmoAddresses[1];
            String mgaddr = InfinitiveAmmoAddresses[2];
            String shotgunaddr = InfinitiveAmmoAddresses[3];
            String rpgaddr = InfinitiveAmmoAddresses[4];
            String sniperaddr = InfinitiveAmmoAddresses[5];

            if(pammo == 0 && sammo == 0 && mammo == 0 && shammo == 0 && rammo == 0 &&  snammo == 0)
            {
                this.pammo = this.GetMemory().ReadFloat(pistoladdr, "", false);
                this.sammo = this.GetMemory().ReadFloat(smgaddr, "", false);
                this.mammo = this.GetMemory().ReadFloat(mgaddr, "", false);
                this.shammo = this.GetMemory().ReadFloat(shotgunaddr, "", false);
                this.rammo = this.GetMemory().ReadFloat(rpgaddr, "", false);
                this.snammo = this.GetMemory().ReadFloat(sniperaddr, "", false);
            }

            this.GetMemory().WriteMemory(pistoladdr, "float", this.pammo+"");
            this.GetMemory().WriteMemory(smgaddr, "float", this.sammo + "");
            this.GetMemory().WriteMemory(mgaddr, "float", this.mammo + "");
            this.GetMemory().WriteMemory(shotgunaddr, "float", this.shammo + "");
            this.GetMemory().WriteMemory(rpgaddr, "float", this.rammo + "");
            this.GetMemory().WriteMemory(sniperaddr, "float", this.snammo + "");
        }


        public String YCoordAddress
        {
            get;set;
        }

        public bool IsYCoordAddress()
        {
            if(YCoordAddress != null)
            {
                return true;
            }
            return false;
        }


        public Location Location
        {
            get
            {
                if(!IsYCoordAddress())
                {
                    throw new System.Exception("no base address for the y coordinate base has been set.");
                }
                ulong yaddress = this.GetMemory().GetCode(YCoordAddress).ToUInt64();
                ulong zaddress = yaddress - 4;
                ulong xaddress = zaddress - 4;

                float x = this.GetMemory().ReadFloat(xaddress.ToString("X4"), "", false);
                float y = this.GetMemory().ReadFloat(yaddress.ToString("X4"), "", false);
                float z = this.GetMemory().ReadFloat(zaddress.ToString("X4"), "", false);

                return new Location(x, y, z);
            }
        }

        public String MoneyAddress
        {
            get;set;
        }

        public bool IsMoneyAddress()
        {
            if(MoneyAddress != null)
            {
                return true;
            }
            return false;
        }

        public void GiveMoney(int amount)
        {
            if(!IsMoneyAddress())
            {
                throw new System.Exception("no base address for the money base has been set.");
            }

            int money = this.GetMemory().ReadInt(MoneyAddress);

            this.GetMemory().WriteMemory(MoneyAddress, "int", money + amount + "");

        }

        public String EridiumAddress
        {
            get;set;
        }

        public bool IsEridiumAddress()
        {
            if(EridiumAddress != null)
            {
                return true;
            }
            return false;
        }

        public void GiveEridium(int amount)
        {
            if(!this.IsEridiumAddress())
            {
                throw new System.Exception("no eridium base address has been set");
            }

            int a = this.GetMemory().ReadInt(this.EridiumAddress);
            this.GetMemory().WriteMemory(this.EridiumAddress, "int", a+amount + "");
        }

        public String GoldenKeyAddress
        {
            get;set;
        }

        public bool IsGoldenKeyAddress()
        {
            if(GoldenKeyAddress != null)
            {
                return true;
            }
            return false;
        }

        public void GiveGoldenKey(int amount)
        {
            if(!IsGodmodeAddress())
            {
                throw new System.Exception("no golden key base address has been set!");
            }

            int keys = this.GetMemory().ReadInt(this.GoldenKeyAddress);

            this.GetMemory().WriteMemory(this.GoldenKeyAddress, "int", keys + amount + "");
        }

        public void TeleportTo(TeleportType type)
        {
            if (!IsYCoordAddress())
            {
                throw new System.Exception("no base address for the y coordinate base has been set.");
            }

            ulong yaddress = this.GetMemory().GetCode(YCoordAddress).ToUInt64();
            ulong zaddress = yaddress - 4;
            ulong xaddress = zaddress - 4;

            this.lastLocation = this.Location;

            this.GetMemory().WriteMemory(xaddress.ToString("X4"), "float", type.GetX());
            this.GetMemory().WriteMemory(yaddress.ToString("X4"), "float", type.GetY());
            this.GetMemory().WriteMemory(zaddress.ToString("X4"), "float", type.GetZ());

        }

        public void UndoTeleport()
        {
            if (!IsYCoordAddress())
            {
                throw new System.Exception("no base address for the y coordinate base has been set.");
            }

            if (this.lastLocation == null)
            {
                Console.Beep();
                return;
            }

            ulong yaddress = this.GetMemory().GetCode(YCoordAddress).ToUInt64();
            ulong zaddress = yaddress - 4;
            ulong xaddress = zaddress - 4;

            this.GetMemory().WriteMemory(xaddress.ToString("X4"), "float", this.lastLocation.GetX() +"");
            this.GetMemory().WriteMemory(yaddress.ToString("X4"), "float", this.lastLocation.GetY()+"");
            this.GetMemory().WriteMemory(zaddress.ToString("X4"), "float", this.lastLocation.GetZ()+"");
        }

    }
}
