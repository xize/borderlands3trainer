﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Borderlands3Trainer.cheats
{
    public class CheatDataType
    {
        private static HashSet<CheatDataType> data = new HashSet<CheatDataType>();

        public static CheatDataType HEALTH = new CheatDataType("health", "godmode", ActionType.TOGGLE, CheatOffsetType.HEALTH);
        public static CheatDataType SHIELD = new CheatDataType("shield", "shield", ActionType.TOGGLE, CheatOffsetType.SHIELD);

        public CheatDataType(String name, String description, ActionType action, CheatOffsetType offset)
        {
            this.Name = name;
            this.Description = description;
            this.Action = action;
            this.Data = offset;
            data.Add(this);
        }

        /**
         * <summary>the name of the cheat</summary>
         */
        public String Name
        {
            get;private set;
        }

        /**
         * <summary>the description of the cheat (this should get in the trainer)</summary>
         */
        public String Description
        {
            get;private set;
        }

        /**
         * <summary>the action if the content should be a button or a toggle or numberic with button</summary>
         */
        public ActionType Action
        {
            get;private set;
        }

        /**
         * <summary>the offset data (this might not be accessed though)</summary>
         */
        public CheatOffsetType Data
        {
            get;private set;
        }

        public static CheatDataType[] Values
        {
            get { return data.ToArray(); }
        }

        public enum ActionType
        {
            BUTTON,
            TOGGLE,
            NUMBERIC_BUTTON,
            TELEPORT_BUTTON
        }

    }
}
