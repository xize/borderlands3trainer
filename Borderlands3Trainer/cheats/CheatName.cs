﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Borderlands3Trainer.cheats
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Property, Inherited = false)]
    public class CheatName : Attribute
    {
        private String name;

        public CheatName(String name)
        {
            this.name = name;
        }

        /**
         * <summary>
         * returns name of the origins cheat this offset comes from
         * </summary>
         * */
        public String Name
        {
            get { return this.name; }
        }

    }
}
