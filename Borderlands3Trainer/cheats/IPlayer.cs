﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Borderlands3Trainer.cheats
{
    /**
     * <summary>presents the player cheat class which can be used for cheats.</summary>
     */
    public interface IPlayer
    {

        /**
         * <summary>sets the godmode address after a checkbox has been set when the aob scan was successfull</summary>
         */
        String GodmodeAddress
        {
            get;set;
        }

        /**
         * <summary>returns true if godmode is still running on a base address</summary>
         */
        bool IsGodmodeAddress();

        /**
         * <summary>fires the godmode script.</summary>
         */
        void DoGodmode();

        /**
         * <summary>sets the infinitive shield once the checkbox has been checked and when the aob scan has been succeeded</summary>
         */
        String InfinitiveShieldAddress
        {
            get; set;
        }

        /**
         * <summary>returns true when the infinitive shield base address exists</summary>
         */
        bool IsInfinitiveShieldAddress();

        /**
         * <summary>runs the infinitive shield  script</summary>
         */
        void DoInfinitiveShield();

        /**
         * <summary>sets the infinitive grenade base address after the checkbox has been set and the aob scan succeeded</summary>
         */
        String InfinitiveGrenadeAddress
        {
            get;set;
        }

        /**
         * <summary>returns true when the base address exists for infinitive grenades</summary>
         */
        bool IsInfinitiveGrenadeAddress();

        /**
         * <summary>runs the infinitive grenades script</summary>
         */
        void DoInfinitiveGrenades();

        /**
         * <summary>sets the infinitive ammo memory addresses obtained from the aob scans when the checkbox got checked.</summary>
         */
        String[] InfinitiveAmmoAddresses
        {
            get;set;
        }

        /**
         * <summary>returns true if the infinitive ammo has base addresses</summary>
         */
        bool IsInfinitiveAmmoAddresses();

        /**
         * <summary>run the infinitive ammo script</summary>
         */
        void DoInfinitiveAmmo();

        /**
         * <summary>gets or set the y coord base address</summary>
         */
        String YCoordAddress
        {
            get;set;
        }

        /**
         * <summary>returns true if the y coord has a base address</summary>
         */
        bool IsYCoordAddress();

        /**
         * <summary>gets the location of the player</summary>
         */
        Location Location
        {
            get;
        }

        /**
         * <summary>gets or sets the money base address</summary>
         */
        String MoneyAddress
        {
            get;set;
        }

        /**
         * <summary>returns true if the base address for money has been set</summary>
         */
        bool IsMoneyAddress();

        /**
         * <summary>gives money</summary>
         * <param name="amount">the amount of money</param>
         */
        void GiveMoney(int amount);

        /**
         * <summary>sets or gets the base address for eridium</summary>
         */
        String EridiumAddress
        {
            get;set;
        }

        /**
         * <summary>returns true if the eridium base address has been set</summary>
         */
        bool IsEridiumAddress();

        /**
         * <summary>gives eridium</summary>
         * <param name="amount">the amount of eridium</param>
         */
        void GiveEridium(int amount);

        /**
         * <summary>gets or sets the golden key base address</summary>
         */
        String GoldenKeyAddress
        {
            get;set;
        }

        /**
         * <summary>returns true if the golden key base address has been set</summary>
         */
        bool IsGoldenKeyAddress();

        /**
         * <summary>gives golden keys</summary>
         * <param name="amount">the amount of golden keys being given</param>
         */
        void GiveGoldenKey(int amount);

        /**
         * <summary>teleports to a location via TeleportType</summary>
         * <param name="type">the teleport location, this can also be a CustomTeleportType ;-)</param>
         */
        void TeleportTo(TeleportType type);

        /**
         * <summary>teleports back to the last location</summary>
         */
        void UndoTeleport();

    }
}
