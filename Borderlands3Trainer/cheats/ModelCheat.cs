﻿using Borderlands3Trainer.controls;
using Memory;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Borderlands3Trainer.cheats
{
    public abstract class ModelCheat
    {

        private Mem m;
        public ModelCheat(Mem m)
        {
            this.m = m;
        }

        public Mem GetMemory()
        {
            return this.m;
        }

    }
}
