﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Borderlands3Trainer.cheats 
{
    public class CheatOffsetType
    {
        public static HashSet<CheatOffsetType> types = new HashSet<CheatOffsetType>();

        public static CheatOffsetType HEALTH = new CheatOffsetType("health", "?? ?? ?? 4? ?? ?? ?? ?? ?? ?? ?? ?? 3E AF 70 46 00 00");
        public static CheatOffsetType SHIELD = new CheatOffsetType("shield", "?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 12 BD 99 46 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 40");
        public static CheatOffsetType GRENADES = new CheatOffsetType("grenades", "?? 00 ?0 41 00 00 00 00 00 00 00 00 00 00 30 41 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00");
        public static CheatOffsetType AMMO = new CheatOffsetType("ammo",
            new String[] {
                //taken from the inventory icon list left to right.
                "00 00 7? 44 00 00 00 00 00 00 00 00 00 00 7A 44 00 00 00 00 00 00 00 00 00 00 00 00 00 00", //pistol
                "00 ?0 D? 44 00 00 00 00 00 00 00 00 00 00 E1 44", //smg
                "00 ?0 A? 44 00 00 00 00 00 00 00 00 00 00 AF 44 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00", //mg
                "00 00 ?? 43 00 00 00 00 00 00 00 00 00 00 70 43 00 00 00 00 00 00 00 00 00 00 00 00 00 00", //shotgun
                "00 00 F8 41 00 00 00 00 00 00 00 00 00 00 10 42 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00", //rpg
                "00 00 ?? 43 00 00 00 00 00 00 00 00 00 00 10 43" //sniper
        });
        //public static CheatOffsetType AMMO = new CheatOffsetType("ammo", "0x30,0x78,0x848,0x478,0x550,0x68,0x180,0x384");
        public static CheatOffsetType Y_COORD = new CheatOffsetType("ycoord", "?? ?? ?? 44 00 00 00 00 00 00 80 3F 00 00 80 3F 00 00 80 3F 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 E1 10 02 00 00 00 00 00 00 00 00 80 ?? ");
        public static CheatOffsetType MONEY = new CheatOffsetType("money", "?? 51 8F 03 E2 D3 E5 20 00 00 00 00 00 00 00 00 00 22 82 6B 01 00 00 00 01 EB 8C 00");
        public static CheatOffsetType ERIDIUM = new CheatOffsetType("eridium", "?? B1 0F 00 00 00 00 00 00 00 00 00 00 00 00 00 00 02 8C 01 02 00 00 00 01 FF A8 53");
        public static CheatOffsetType GOLDEN_KEYS = new CheatOffsetType("golden_keys", "?? 58 B8 00 00 00 00 00 10 2A C9 80 00 00 00 00 39 04 00 00 00 00 00 00 50 21 C9");


        private String name;
        private String offset;
        private String[] offsets;

        private CheatOffsetType(String name, String offset, bool isAOB = true)
        {
            this.name = name;
            this.offset = offset;
            AOB = isAOB;
            types.Add(this);
        }

        private CheatOffsetType(String name, String[] offsets, bool isAOB = true)
        {
            this.name = name;
            this.offsets = offsets;
            AOB = isAOB;
            types.Add(this);
        }

        /**
         * <summary>the name of the cheat</summary>
         **/
        public String Name
        {
            get { return this.name; }
        }

        /**
         * <summary>returns true if the cheat has a single offset, false when the cheat has more offsets</summary>
         */
        public bool SingleOffset
        {
            get
            {
                if (this.offset != null)
                {
                    return true;
                }
                return false;
            }
        }

        /**
         * <summary>returns the cheat offset</summary>
         */
        public String Offset
        {
            get { return this.offset; }
        }

        /**
         * <summary>returns multiple offsets in a form of a array</summary>
         */
        public String[] Offsets
        {
            get { return this.offsets; }
        }

        public bool AOB
        {
            get; private set;
        }

        public static CheatOffsetType[] Values
        {
            get { return types.ToArray(); }
        }

        public static CheatOffsetType ValueOf(String name)
        {
            foreach (CheatOffsetType offsetm in Values)
            {
                if (name.ToLower().StartsWith(offsetm.Name))
                {
                    return offsetm;
                }
            }
            throw new System.Exception("cannot find value with this name in CheatOffsetType: " + name);
        }
    }
}
