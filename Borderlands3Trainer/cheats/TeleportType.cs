﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Borderlands3Trainer.cheats
{
    public class TeleportType
    {
        protected static HashSet<TeleportType> types = new HashSet<TeleportType>();

        public static TeleportType EDEN_SIX_MERIDIAN_METROCOMPLEX_GIGAMIND = new TeleportType(
            "MERIDIAN_METROCOMPLEX -> [Gigamind]",
            "to the gigamind boss inside meridian metroplex",
            "" + -19089.67,
            "" + 5129.686,
            "" + 237.2049);

        public static TeleportType EDEN_SIX_THE_ANVIL_WARDEN = new TeleportType(
        "EDEN_SIX_THE_ANVIL -> [warden]",
        "to the warden boss in eden six the anvil in Eden-6",
        "" + 30891.54,
        "" + 9192.282,
        "" + 22166.58);

        public static TeleportType EDEN_SIX_JACOB_REAL_ESTATE_DADDY = new TeleportType(
        "EDEN_SIX_JACOB_REAL_ESTATE_DADDY -> [Daddy bounty]",
        "to the daddy bounty inside Eden-6",
        "" + -34045.71,
        "" + 1431.481,
        "" + -17405.41);

        public static TeleportType EDEN_SIX_JACOB_REAL_ESTATE_ELDRAGON = new TeleportType(
        "EDEN_SIX_JACOB_REAL_ESTATE_ELDRAGON -> [E'lDragon bounty]",
        "to E'l dragon bounty inside Eden-6",
        "" + -34728.01,
        "" + -108.9239,
        "" + -5562.786);

        public static TeleportType THE_HANDSOME_JACKPOT_COMPACTOR_SCRAPTRAP = new TeleportType(
        "THE_HANDSOME_JACKPOT_COMPACTOR_SCRAPTRAP -> [Scraptrap boss]",
        "to the scraptrap boss inside the compactor",
        ""+ 43571.24,
        ""+ 614.4153,
        ""+ -13660.03);

        public static TeleportType ATHENAS_CAPTAIN_TRAUNT = new TeleportType(
        "ATHENAS_CAPTAIN_TRAUNT -> [Captrain Traunt boss]",
        "to the captain traunt boss in athenas",
        "" + -72530.66,
        "" + 12477.36,
        "" + -26396.56);

        public static TeleportType MIDNIGHT_CARIN_VALKYRIE_BOSS = new TeleportType(
        "MIDNIGHT_CARIN_VALKYRIE_BOSS -> [Valyrie boss]",
        "to the valkyrie boss in midnight carin",
        "" + 9287.908,
        "" + 3554.631,
        "" + -22376.54);

        public static TeleportType MIDNIGHT_CARIN_WOTAN_BOSS = new TeleportType(
        "MIDNIGHT_CARIN_WOTAN_BOSS -> [Wotan boss]",
        "to the wotan boss in midnight carin",
        "" + 3552.97,
        "" + -8930.368,
        "" + -64762.56);


        public static TeleportType THE_HOUSE_THAT_JOEY_BUILD_BOSS = new TeleportType(
        "THE_HOUSE_THAT_JOEY_BUILD -> [main entrance (Cartel event)]",
        "to the house main entrance that joey build",
        "" + 1455.888,
        "" + 1262.25,
        "" + 170.4727);

        public static TeleportType THE_HOUSE_THAT_JOEY_BUILD_SECRET_ROOM = new TeleportType(
        "THE_HOUSE_THAT_JOEY_BUILD_SECRET -> [secret room]",
        "to the secret room in the house that joey build",
        "" + 5275.623,
        "" + 2176.415,
        "" + 1659.063);

        private String name;
        private String voicename;
        private String x;
        private String y;
        private String z;
        protected bool isCustom = false;

        public TeleportType(String name, String voicename, String x, String y, String z, bool isCustom = false)
        {
            this.name = name;
            this.isCustom = false;
            this.voicename = voicename;
            this.x = x;
            this.y = y;
            this.z = z;

            if(this.isCustom)
            {
                foreach(TeleportType type in types)
                {
                    if(type.Equals(this))
                    {
                        types.Remove(type);
                        break;
                    }
                }
            }

            types.Add(this);
        }

        public String GetName()
        {
            return this.name;
        }

        public String getVoiceName()
        {
            return this.voicename;
        }

        public String GetX()
        {
            return this.x;
        }

        public String GetY()
        {
            return this.y;
        }

        public String GetZ()
        {
            return this.z;
        }

        public static TeleportType[] Values()
        {
            return types.ToArray();
        }

        public static TeleportType ValueOf(String name)
        {
            IEnumerator<TeleportType> it = TeleportType.Values().Cast<TeleportType>().GetEnumerator();

            while(it.MoveNext())
            {
                TeleportType type = it.Current;
                if(type.GetName().ToLower().Equals(name.ToLower()))
                {
                    return type;
                }
            }

            throw new System.Exception("invalid value "+name+"!");
        }

        public override bool Equals(object obj)
        {
            if(this.GetName().Equals(((TeleportType)obj).GetName()))
            {
                return true;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return (this.GetName() + this.isCustom).GetHashCode();
        }
    }
}
