﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands3Trainer
{
    public partial class WaitWindow : Form
    {
        public WaitWindow()
        {
            InitializeComponent();
        }

        public void DisableButton()
        {
            this.button1.Enabled = false;
        }

        public void EnableButton()
        {
            this.button1.Enabled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
