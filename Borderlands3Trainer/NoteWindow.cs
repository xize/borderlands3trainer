﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands3Trainer
{
    public partial class NoteWindow : Form
    {
        public NoteWindow()
        {
            InitializeComponent();
        }

        private void NoteWindow_Load(object sender, EventArgs e)
        {
            this.moveAbleWindow1.ControlTarget = new Control[] { this.panel1 };
            this.fadeControl1.Start();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
