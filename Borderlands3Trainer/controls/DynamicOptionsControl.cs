﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Borderlands3Trainer.cheats;

namespace Borderlands3Trainer.controls
{
    public partial class DynamicOptionsControl : UserControl
    {
        public DynamicOptionsControl()
        {
            InitializeComponent();
        }

        public void Render()
        {
            foreach(CheatDataType type in CheatDataType.Values)
            {
                this.table.RowStyles.Add(new RowStyle(SizeType.Absolute, 16F));
                this.table.RowCount++;

                foreach (Control c in this.table.Controls)
                {
                    this.table.SetRow(c, this.table.GetRow(c) + 1);
                }

                table.Controls.Add(new Label() { Text = type.Description }, 0, 0);
                switch(type.Action)
                {
                    case CheatDataType.ActionType.BUTTON:
                        table.Controls.Add(new Button() { Text = "click" }, 1, 0);
                        break;
                    case CheatDataType.ActionType.NUMBERIC_BUTTON:
                        table.Controls.Add(new Button() { Text = "[custom control]..." }, 1, 0);
                        break;
                    case CheatDataType.ActionType.TELEPORT_BUTTON:
                        table.Controls.Add(new Button() { Text = "tp" }, 1, 0);
                        break;
                    case CheatDataType.ActionType.TOGGLE:
                        table.Controls.Add( new CustomCheckBox(), 1, 0);
                        break;
                }
            }
        }

    }
}
