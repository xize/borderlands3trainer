﻿namespace Borderlands3Trainer.controls
{
    partial class WaitControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.waiter = new System.Windows.Forms.Label();
            this.worker = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // waiter
            // 
            this.waiter.AutoSize = true;
            this.waiter.BackColor = System.Drawing.Color.Transparent;
            this.waiter.Dock = System.Windows.Forms.DockStyle.Top;
            this.waiter.Font = new System.Drawing.Font("Yu Gothic UI", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.waiter.ForeColor = System.Drawing.Color.White;
            this.waiter.Location = new System.Drawing.Point(0, 0);
            this.waiter.Name = "waiter";
            this.waiter.Size = new System.Drawing.Size(16, 13);
            this.waiter.TabIndex = 0;
            this.waiter.Text = "...";
            // 
            // worker
            // 
            this.worker.WorkerReportsProgress = true;
            this.worker.WorkerSupportsCancellation = true;
            this.worker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.dowork);
            this.worker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.progress);
            // 
            // WaitControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.waiter);
            this.Name = "WaitControl";
            this.Size = new System.Drawing.Size(15, 16);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label waiter;
        private System.ComponentModel.BackgroundWorker worker;
    }
}
