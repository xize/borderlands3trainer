﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands3Trainer.controls
{
    public partial class WaitControl : UserControl
    {

        private bool cancel = false;

        public WaitControl()
        {
            InitializeComponent();
        }

        public Color DotColor
        {
            get
            {
                return this.waiter.ForeColor;
            }
            set
            {
                this.waiter.ForeColor = value;
            }
        }

        public void Start()
        {
            this.worker.RunWorkerAsync();
        }

        public void Stop()
        {
            this.cancel = true;
            this.worker.CancelAsync();
        }

        private void dowork(object sender, DoWorkEventArgs e)
        {
            int dots = 3;
            while(true)
            {
                if(this.cancel)
                {
                    break;
                }

                if(dots == 1)
                {
                    this.worker.ReportProgress(1);
                    dots = 2;
                } else if(dots == 2)
                {
                    this.worker.ReportProgress(2);        
                    dots = 3;
                } else if(dots == 3)
                {
                    this.worker.ReportProgress(3);
                    dots = 1;
                }

                Task t = Task.Delay(500);
                t.Wait();
            }
        }

        private void progress(object sender, ProgressChangedEventArgs e)
        {
            if(e.ProgressPercentage == 1)
            {
                this.waiter.Text = ".";
            } else if(e.ProgressPercentage == 2)
            {
                this.waiter.Text = "..";
            } else if(e.ProgressPercentage == 3)
            {
                this.waiter.Text = "...";
            }
        }
    }
}
