﻿using Borderlands3Trainer.controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Borderlands3Trainer.cheats
{
    public class CustomCheckBoxEventArgs
    {

        private CustomCheckBox.CheckState current;
        private CustomCheckBox.CheckState previous;

        public CustomCheckBoxEventArgs(CustomCheckBox.CheckState state)
        {
            if(state == CustomCheckBox.CheckState.CHECKED)
            {
                this.current = state;
                this.previous = CustomCheckBox.CheckState.UNCHECKED;
            } else
            {
                this.current = state;
                this.previous = CustomCheckBox.CheckState.CHECKED;
            }
        }

        public bool IsCancelled()
        {
            return Cancel;
        }

        public CustomCheckBox.CheckState CurrentState()
        {
            return this.current;
        }

        public CustomCheckBox.CheckState PreviousState()
        {
            return this.previous;
        }

        public bool Cancel
        {
            get; set;
        }

    }
}
