﻿using Borderlands3Trainer.cheats;
using Borderlands3Trainer.util;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Borderlands3Trainer.controls
{
    public class CustomCheckBox : UserControl
    {

        /*
         *                 if (value)
                {
                    this.ball.Location = new System.Drawing.Point(12, 2);
                    this.ball.BackgroundImage = Properties.Resources.checkboxball_black_active1;
                    this.BackgroundImage = Properties.Resources.checkbox_black1;
                }
                else
                {
                    this.ball.BackgroundImage = Properties.Resources.checkboxball_black1;
                    this.ball.Location = new System.Drawing.Point(5, 2);
                    this.BackgroundImage = Properties.Resources.checkbox_black1;
                }
                this.Update();
                */

        private Panel ball;

        public event EventHandler<CustomCheckBoxEventArgs> CustomCheckBoxChange;

        public CustomCheckBox()
        {
            this.InitializeComponent();
        }

        private void InitializeComponent()
        {
            this.ball = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // ball
            // 
            this.ball.BackgroundImage = global::Borderlands3Trainer.Properties.Resources.checkboxball_black1;
            this.ball.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ball.Location = new System.Drawing.Point(11, 2);
            this.ball.Name = "ball";
            this.ball.Size = new System.Drawing.Size(8, 8);
            this.ball.TabIndex = 0;
            this.ball.Click += new System.EventHandler(this.OnActivateCheckBoxAsync);
            // 
            // CustomCheckBox
            // 
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImage = global::Borderlands3Trainer.Properties.Resources.checkbox_black1;
            this.Controls.Add(this.ball);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.MaximumSize = new System.Drawing.Size(23, 12);
            this.MinimumSize = new System.Drawing.Size(23, 12);
            this.Name = "CustomCheckBox";
            this.Size = new System.Drawing.Size(23, 12);
            this.Click += new System.EventHandler(this.OnActivateCheckBoxAsync);
            this.ResumeLayout(false);

        }

        public void PerformClick()
        {
           OnActivateCheckBoxAsync(this, EventArgs.Empty);
        }

        public bool Checked { get; set; } = false;

        private async void OnActivateCheckBoxAsync(object sender, EventArgs e)
        {
            CustomCheckBoxEventArgs args = null;
            EventHandler<CustomCheckBoxEventArgs> handler = CustomCheckBoxChange;
            if (Checked)
            {
                //we go to unchecked
                args = new CustomCheckBoxEventArgs(CheckState.UNCHECKED);
            } else
            {
                //we go to checked
                args = new CustomCheckBoxEventArgs(CheckState.CHECKED);
            }

            if(handler != null)
            {
                this.Invoke(new MethodInvoker(delegate
                {
                    this.Enabled = false;
                    this.FindForm().Cursor = Cursors.WaitCursor;
                }));
                await Task.Run(() => handler?.Invoke(sender, args));
                

                if(args.CurrentState().Equals(CheckState.CHECKED))
                {
                    this.Invoke(new MethodInvoker(delegate
                    {
                        this.Checked = true;
                        Console.Beep(500, 100);
                        Console.Beep(900, 100);
                        Console.Beep(100, 100);
                        //Console.Beep(800, 500);
                        this.ball.Location = new System.Drawing.Point(12, 2);
                        this.ball.BackgroundImage = Properties.Resources.checkboxball_black_active1;
                        this.BackgroundImage = Properties.Resources.checkbox_black1;
                    }));
                } else
                {
                    this.Invoke(new MethodInvoker(delegate
                    {
                        this.Checked = false;
                        Console.Beep(200, 100);
                        Console.Beep(300, 100);
                        Console.Beep(100, 100);
                        this.ball.BackgroundImage = Properties.Resources.checkboxball_black1;
                        this.ball.Location = new System.Drawing.Point(5, 2);
                        this.BackgroundImage = Properties.Resources.checkbox_black1;
                    }));
                }
                this.Invoke(new MethodInvoker(delegate
                {
                    this.Enabled = true;
                    this.FindForm().Cursor = Cursors.Default;
                }));
            }
        }


        public enum CheckState
        {
            CHECKED,
            UNCHECKED
        }

    }
}
